/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.texture.Texture;

/**
 *
 * @author marko
 */
public class BlockMaterial extends Material {

    public BlockMaterial(AssetManager assetManager){
        super(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        
        Texture texture = assetManager.loadTexture("Textures/grass.png");
        texture.setMagFilter(Texture.MagFilter.Nearest);
        texture.setMinFilter(Texture.MinFilter.NearestNoMipMaps);
        super.setTexture("ColorMap", texture);
        super.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
       // super.getAdditionalRenderState().setWireframe(true);
    }
    
}
