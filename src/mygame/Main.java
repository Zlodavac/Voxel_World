package mygame;

import DB.LocalDatabase;
import mygame.Basic.Voxel;
import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.ssao.SSAOFilter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.scene.shape.Sphere;
import com.jme3.shadow.DirectionalLightShadowFilter;
import com.jme3.shadow.DirectionalLightShadowRenderer;
import com.jme3.shadow.PointLightShadowRenderer;

import com.jme3.texture.Texture;
import com.jme3.texture.Texture.WrapMode;
import com.jme3.util.BufferUtils;
import com.jme3.util.TangentBinormalGenerator;
import com.simsilica.lemur.Button;
import com.simsilica.lemur.Command;
import com.simsilica.lemur.Container;
import com.simsilica.lemur.GuiGlobals;
import com.simsilica.lemur.Label;
import com.simsilica.lemur.TextField;
import com.simsilica.lemur.style.BaseStyles;
import com.simsilica.lemur.style.ElementId;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import mygame.Basic.Spiral;
import mygame.Terrain.BlockTerrain;
import mygame.Terrain.CallableSurface;
import mygame.Terrain.Noise;
import mygame.Terrain.World;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication implements ActionListener {


      public static void main(String[] args) {
        Main app = new Main();
        app.start();
        
    }
      LocalDatabase DB = LocalDatabase.getInstance();
     public static ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(4);
     Spiral spiral = new Spiral(10,10);
     float lasttime=0.f;
DirectionalLight sun = new DirectionalLight();
float a =0f;
Player pl = new Player();
PointLight sun_light = new PointLight();
// FractalTerain terr;
 BlockTerrain BT;
//Container myWindow;
GuiWindow gui ;
private Future Future = null;
boolean ExpandTerrain = true;
World w;
@Override
    public void simpleInitApp() {
      // Terrain ter = new Terrain();
      GuiGlobals.initialize(this);
      BaseStyles.loadGlassStyle();
      GuiGlobals.getInstance().getStyles().setDefaultStyle("glass");
   
//     myWindow = new Container();
     gui  = new GuiWindow();
     gui.setName("gui");
   //  guiNode.attachChild(gui);
     flyCam.setMoveSpeed(1000f);
     
      cam.setFrustumFar(100000);

       cam.onFrameChange();

 //Put it somewhere that we will see it.
// Note: Lemur GUI elements grow down from the upper left corner.
  gui.setLocalTranslation(100, 900, 0);
  w = new World(assetManager);
  rootNode.addControl(w);
// Add some elements
/*
myWindow.addChild(new Label("terrain options"));
myWindow.addChild(new Label("e1"));
myWindow.addChild(new TextField("1", new ElementId ("e1")));
myWindow.addChild(new Label("e2"));
myWindow.addChild(new TextField("2", new ElementId ("e2")));
myWindow.addChild(new Label("e3"));
myWindow.addChild(new TextField("3", new ElementId ("e3")));
myWindow.addChild(new Label("e4"));
myWindow.addChild(new TextField("4", new ElementId ("e4")));
myWindow.addChild(new Label("e5"));
myWindow.addChild(new TextField("5", new ElementId ("e5")));
myWindow.addChild(new Label("e6"));
myWindow.addChild(new TextField("6", new ElementId ("e6")));


Button clickMe = myWindow.addChild(new Button("Click Me"));
clickMe.addClickCommands(new Command<Button>() {
        @Override
        public void execute( Button source ) {
            //double e1 = myWindow.getChild(INPUT_MAPPING_EXIT)
            
            
            
            flyCam.setEnabled(true);
            inputManager.setCursorVisible(false);
            
        guiNode.detachAllChildren();
        }
         });
*/
      this.initControls();
      flyCam.setEnabled(true);
      inputManager.setCursorVisible(true);
       // terr = new FractalTerain(assetManager);
//        terr.build();
        Node terrnode = new Node();
//        terrnode.addControl(terr);
        rootNode.attachChild(terrnode);
        
         BT = new BlockTerrain(assetManager);
        rootNode.attachChild(BT.getSpatial());
        Mesh mterr = new Mesh();
        Mesh m = new Mesh();
        Mesh m2 = new Mesh();
        Mesh m3 = new Mesh();
        //Tree t= new Tree().BuildTree(4f).BuildTree(assetManager);
       // t.CreateMesh();
        Mesh mV = new Mesh();
        Voxel vox = new Voxel(1f);
         pl = new Player(assetManager);
     //   m.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(ter.getVertex()));
      //  m2.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(ter.getFaceVertex()));
      //  m3.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(ter.getFaceVertex()));
     //   m3.setBuffer(Type.Index, 1, BufferUtils.createIntBuffer(ter.getIndex()));
        mV.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(vox.getVertex()) );
        mV.setBuffer(Type.Index, 1, BufferUtils.createIntBuffer(vox.getIndex()) );
        mV.setBuffer(Type.Normal, 3, BufferUtils.createFloatBuffer(vox.getNormals()) );
        mV.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(vox.getText()) );
        
       
        m.setMode(Mesh.Mode.Points);
        m2.setMode(Mesh.Mode.Points);
        m3.setMode(Mesh.Mode.Triangles);
         mV.setMode(Mesh.Mode.Triangles);
        m.updateBound();
        m2.updateBound();
        m3.updateBound();
        mV.updateBound();
        m.setPointSize(10);
        m2.setPointSize(15);
        mterr.updateBound();
        mterr.setStatic();
        Geometry geom = new Geometry("Box", m);
        Geometry geom2 = new Geometry("Box", m2);
        Geometry geom3 = new Geometry("Box", m3);
        Geometry geom4 = new Geometry("Box", mV);
        
        Geometry FractTerr = new Geometry("teren",mterr);
        
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
       // mat.setColor("Color", ColorRGBA.Blue);
        Material matt = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
       // matt.setTexture("Alpha", assetManager.loadTexture(
       //     "Textures/Terrain/splat/alphamap.png"));
        Texture grass = assetManager.loadTexture(
            "Textures/grass.png");
        Texture dirt = assetManager.loadTexture(
            "Textures/stone.png");
        Texture sky = assetManager.loadTexture(
            "Textures/cubes/sky.jpg");
            grass.setWrap(WrapMode.Repeat);
            dirt.setWrap(WrapMode.Repeat);
         TangentBinormalGenerator.generate(FractTerr);
         //TangentBinormalGenerator.generate(sphereMesh);           // for lighting effect
    Material sphereMat1 = new Material(assetManager,
        "Common/MatDefs/Light/Lighting.j3md");
    sphereMat1.setTexture("DiffuseMap",
        grass);
  //  sphereMat1.setTexture("NormalMap",
    //    dirt);
    sphereMat1.setBoolean("UseMaterialColors",true);
    sphereMat1.setColor("Diffuse",ColorRGBA.White);
    sphereMat1.setColor("Specular",ColorRGBA.White);
    sphereMat1.setFloat("Shininess", 0f);  // [0,128]
    
    FractTerr.setShadowMode(ShadowMode.CastAndReceive);
         
         Material sphereMat = new Material(assetManager,
        "Common/MatDefs/Light/Lighting.j3md");
    sphereMat.setTexture("DiffuseMap",
        assetManager.loadTexture("Textures/grass.png"));
    sphereMat.setTexture("NormalMap",
        assetManager.loadTexture("Textures/grass.png"));
    sphereMat.setBoolean("UseMaterialColors",true);
    sphereMat.setColor("Diffuse",ColorRGBA.White);
    sphereMat.setColor("Specular",ColorRGBA.LightGray);
    sphereMat.setFloat("Shininess", 64f);  // [0,128]
    FractTerr.setMaterial(sphereMat);
    FractTerr.setShadowMode(ShadowMode.CastAndReceive);
        geom.setMaterial(mat);
        geom2.setMaterial(matt);
        geom3.setMaterial(matt);
        geom4.setMaterial(sphereMat1);
       // sphereMat1.getAdditionalRenderState().setWireframe(true);
        geom4.setShadowMode(ShadowMode.CastAndReceive);
       // matt.getAdditionalRenderState().setWireframe(true);
       
      //  FractTerr.getMesh().scaleTextureCoordinates(new Vector2f(20f, 20f));

  
        //matt.getAdditionalRenderState().setBlendMode(BlendMode.Color);
        //rootNode.attachChild(geom);
        //rootNode.attachChild(geom2);
        //rootNode.attachChild(geom3);
        rootNode.attachChild(geom4);
        
        rootNode.attachChild(pl.getSpatial());
        pl.getSpatial().setLocalTranslation(new Vector3f(0f,-12f,0f));
        
      //  FractTerr.updateGeometricState();
        
      //  FractTerr.updateModelBound();
        
       // rootNode.attachChild(FractTerr);
       /* for(Geometry g:t.body)
        {
            g.setShadowMode(ShadowMode.CastAndReceive);
        g.setMaterial(sphereMat);
            rootNode.attachChild(g);
        
        }*/
        
        sun.setColor(ColorRGBA.White.mult(10.5f));
        sun.setDirection(new Vector3f(1f,-.5f,1f).normalizeLocal());
        
        rootNode.addLight(sun);
        AmbientLight ambient = new AmbientLight();
      // FractTerr.addLight(sun);
        ambient.setColor(ColorRGBA.White.mult(10.25f));
          rootNode.addLight(ambient);
           
           
            sun_light.setColor(ColorRGBA.White.mult(80.25f));
            sun_light.setRadius(400f);
            sun_light.setPosition(new Vector3f(10f,100f,10f));
       //  rootNode.addLight(sun_light);
          /** A bumpy rock with a shiny light effect.*/
   /* Sphere sphereMesh = new Sphere(32,32, 2f);
    Geometry sphereGeo = new Geometry("Shiny rock", sphereMesh);
    sphereMesh.setTextureMode(Sphere.TextureMode.Projected); // better quality on spheres
    TangentBinormalGenerator.generate(sphereMesh);           // for lighting effect
    Material sphereMat = new Material(assetManager,
        "Common/MatDefs/Light/Lighting.j3md");
    sphereMat.setTexture("DiffuseMap",
        assetManager.loadTexture("Textures/dirt.png"));
    sphereMat.setTexture("NormalMap",
        assetManager.loadTexture("Textures/grass.png"));
    sphereMat.setBoolean("UseMaterialColors",true);
    sphereMat.setColor("Diffuse",ColorRGBA.White);
    sphereMat.setColor("Specular",ColorRGBA.White);
    sphereMat.setFloat("Shininess", 64f);  // [0,128]
    sphereGeo.setMaterial(sphereMat);
    sphereGeo.setLocalTranslation(0,2,-2); // Move it a bit
    sphereGeo.rotate(1.6f, 0, 0);          // Rotate it a bit
    rootNode.attachChild(sphereGeo); */
    FractTerr.setLocalTranslation(-1,0,-1);   
    
    //shadow
    final int SHADOWMAP_SIZE=1024;
        DirectionalLightShadowRenderer dlsr = new DirectionalLightShadowRenderer(assetManager,SHADOWMAP_SIZE,3);
        dlsr.initialize(renderManager, viewPort);
        dlsr.setLight(sun);
        dlsr.setShadowZFadeLength(5.f);
        dlsr.setShadowZExtend(10f);
       // viewPort.addProcessor(dlsr);

        DirectionalLightShadowFilter dlsf = new DirectionalLightShadowFilter(assetManager, SHADOWMAP_SIZE, 3);
        dlsf.setLight(sun);
        dlsf.setEnabled(true);
        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
        fpp.addFilter(dlsf);
      //  viewPort.addProcessor(fpp);
    
    
  // FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
SSAOFilter ssaoFilter = new   SSAOFilter();            //(12.94f, 43.92f, 0.33f, 0.61f);
fpp.addFilter(ssaoFilter);
//viewPort.addProcessor(fpp); 
    
    
    }

    @Override
    public void simpleUpdate(float tpf) {
        
        
//        try{
//        //If we have no waylist and not started a callable yet, do so!
//        if( this.ExpandTerrain == true && Future == null){
//            //set the desired location vector, after that we should not modify it anymore
//            //because it's being accessed on the other thread!
//            //desiredLocation.set(getGoodNextLocation());
//            //start the callable on the executor
//           // future = executor.submit(findWay);    //  Thread starts!
//           addToQue();
//        }
//        //If we have started a callable already, we check the status
//        else if(Future != null){
//            //Get the waylist when its done
//            if(Future.isDone()){
//                Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//                Spatial n = (Spatial) Future.get();
//                n.setMaterial(mat);
//                rootNode.attachChild(n);
//                Future = null;
//            }
//            else if(Future.isCancelled()){
//                //Set future to null. Maybe we succeed next time...
//                Future = null;
//            }
//        }
//        }
//        catch(Exception e){
//      System.out.println(e.getMessage());
//    }
//    if(ExpandTerrain == true){
//        //.... Success! Let's process the wayList and move the NPC...
//    System.out.println("gotov");
//    ExpandTerrain=false;
//    }
    
        
        
        
      //  this.BT.setPosition(cam.getLocation());
      //  this.BT.createView();
        //TODO: add update code
       /*
        float timeInSec = timer.getTimeInSeconds();
     
        
        if (lasttime < timeInSec+0.5)
        {
            float x = FastMath.sin(a);
            float y = FastMath.cos(a);
           // float z = FastMath.sin(a)*1;
            
         sun.setDirection(new Vector3f(x,-y,1.1f).normalize());
        lasttime = timeInSec;
        a=a+0.01f;
        
       /* System.out.println(a);
        System.out.println(x);
        System.out.println(y);
        
        //if (a>= FastMath.TWO_PI)
          //  a=0;
        }
        /*if (a > 0 && a < FastMath.QUARTER_PI)
        {
            sun.setColor(ColorRGBA.Orange);
        sun.setColor(sun.getColor().multLocal(1.1f));
        }
        if (a > FastMath.QUARTER_PI && a < FastMath.QUARTER_PI + FastMath.HALF_PI )
        {
            sun.setColor(ColorRGBA.White);
        sun.setColor(sun.getColor().multLocal(1.1f));
        }
        if (a > FastMath.QUARTER_PI + FastMath.HALF_PI  )
        {sun.setColor(ColorRGBA.Red);
        sun.setColor(sun.getColor().multLocal(0.9f));
        
        }
    */
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add re.nder code
    }
//}
@Override
      public void onAction(String name, boolean isPressed, float tpf) {
        
        if(name.equals("set_block") && isPressed){
             //System.out.println(String.valueOf(gui.getData().length));
             pl.setParam(gui.getData());
             pl.Color();
//             terr.setParam(gui.getData());
             gui.setStatus();
             Noise.getInstance().setParam(gui.getData());
             BT.regen();
        }
        
        else if(name.equals("remove_block") && isPressed){
       //    pl.setRandom();
           gui.setStatus();
           Noise.getInstance().setParam(gui.getData());
            }
        else if(name.equals("open_dialog") && isPressed){
           if (guiNode.getChild("gui") != null)
                   {
                   guiNode.detachChild(gui); 
                   // guiNode.attachChild(gui);
                   flyCam.setEnabled(true);
                   inputManager.setCursorVisible(false);
                  gui.setStatus();
                  Noise.getInstance().setParam(gui.getData());
                   }
           else
           {
           //guiNode.detachAllChildren();
                flyCam.setEnabled(false);
                inputManager.setCursorVisible(true);
                guiNode.attachChild(gui);
              gui.setStatus();
              Noise.getInstance().setParam(gui.getData());
               
           }
           
           
            }
        if(name.equals("enable_generation") && isPressed){
             //System.out.println(String.valueOf(gui.getData().length));
            if(!w.ExpandTerrain)
                w.clear();
             
             w.ExpandTerrain =!w.ExpandTerrain;
//          
        }
        
    }
      private void initControls(){
        inputManager.addMapping("set_block", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addListener(this, "set_block");
        inputManager.addMapping("remove_block", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        inputManager.addListener(this, "remove_block");
        inputManager.addMapping("open_dialog", new KeyTrigger(KeyInput.KEY_M));
        inputManager.addListener(this, "open_dialog");
        inputManager.addMapping("enable_generation", new KeyTrigger(KeyInput.KEY_G));
        inputManager.addListener(this, "enable_generation");
    }
      private void addToQue()
      {
      Future = executor.submit(new CallableSurface(0,0,80));
      
      
      }
      
      @Override
    public void destroy() {
        super.destroy();
        executor.shutdown();
    }
}

  
  
