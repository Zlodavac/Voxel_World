/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.Mesh.Mode;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;


/**
 *
 * @author marko
 */
public class NoiseTerrainBuilder implements meshReturn {
   
    private final SimplexNoise Noise;
    private final ArrayList<Vector3f> positions = new ArrayList<Vector3f>();
    private double step = 0.1;
    private final int seed = 1665453136;
    double noise1(double nx, double ny) { return Noise.getNoise(nx,ny); }
    private double e1,e2,e3,e4,e5,e6,e7,a,b,c;
    private double offsetX,offsetY;
    private final NoiseTerrain geo = new NoiseTerrain(positions);     
     private Mesh mesh; 
    public void setParam(double... e)
     
    {
       // System.out.println(String.valueOf(e));
     //   for (int i=0 ;i<e.length; i++)
      //  {
    this.e1 = e[0];
    this.e3 = e[1];
    this.e3 = e[2];
    this.e4 = e[3];
    this.e5 = e[4];
    this.e6 = e[5];
     //System.out.println(String.valueOf(step));
    this.e7 = e[6];
    this.step = e[7];
    this.a = e[8];
    this.b = e[9];
    this.c = e[10];
    this.offsetX = e[11];
    this.offsetY = e[12];
      //  }
    
    
    }
    public void generate(int x,int y)
       {
           positions.clear();
       
      for(int i=0;i<x;i++){
        for(int j=0;j<y;j++){
      
       
         double e;
        // e=(1+noise1(nx,ny));
         // System.out.print(String.valueOf(nx) +" "+String.valueOf(ny) );
         //  System.out.println(String.valueOf(e));
            
      double nx = (i*(0.003))+offsetX,///(double)x), 
             ny = (j*(0.003))+offsetY;//(double)y),
             //nz = ((double)j)/step;//(double)y);
        double d = Math.sqrt(200);
       
         
        // e=(1+noise1(nx,ny));
         // System.out.print(String.valueOf(nx) +" "+String.valueOf(ny) );
         //  System.out.println(String.valueOf(e));
            
      e = e1 * noise1(  nx,   ny)
              + e2 * noise1( 2 * nx,  2 * ny) 
              + e3 * noise1( 4 * nx,  4 * ny) 
              + e4 * noise1( 8 * nx,  8 * ny) 
              + e5 * noise1(16 * nx, 16 * ny) 
              + e6 * noise1(32 * nx, 32 * ny);
    //  e = Math.pow(e, Ma);
      e = e + a - (b*Math.pow(d,c));
      //System.out.println("e: "+String.valueOf(e));
      //e = (double)(Math.round(e*10))/10f;
      //System.out.println("e: "+String.valueOf(e));
     // System.out.println(String.valueOf(e));
     e = (int)(e*10); 
     positions.add(new Vector3f((float)i,(float)e,(float)j));
      
       // geo.Regenerate(positions);
        }
       
       }
      
      
      
      
       }
    public ArrayList<Vector3f> generateRange(Vector2f center,int Radius)
       {
            ArrayList<Vector3f> retval = new ArrayList<Vector3f>();
           //positions.clear();
       
     // for(int i=0;i<x;i++){
       // for(int j=0;j<y;j++){
           int x = (int)center.x - Radius; 
           int y = (int)center.y - Radius;
           
           for ( ; x < (center.x + Radius); x++ )
           {
           for ( ; y < (center.y + Radius); y++ )
           {
           double nx = x*0.01f,///(double)x), 
             ny = y*0.01f;//(double)y),
             //nz = ((double)j)/step;//(double)y);
        // double d = Math.sqrt(nx*nx + ny*ny);
       
         double e;
        // e=(1+noise1(nx,ny));
         // System.out.print(String.valueOf(nx) +" "+String.valueOf(ny) );
         //  System.out.println(String.valueOf(e));
            
      e = e1 * noise1(  nx,   ny)
              + e2 * noise1( 2 * nx,  2 * ny) 
              + e3 * noise1( 4 * nx,  4 * ny) 
              + e4 * noise1( 8 * nx,  8 * ny) 
              + e5 * noise1(16 * nx, 16 * ny) 
              + e6 * noise1(32 * nx, 32 * ny);
    //  e = e + a - b*Math.pow(d,c);
      
       e = (double)(Math.round(e))/10f;
       e = (int)(e*10); 
      
     retval.add(new Vector3f((float)x,(float)e,(float)y));
// System.out.println(String.valueOf(e));
       //positions.add(new Vector3f((float)i,(int)e*10,(float)j));
      // geo.Regenerate(positions);
        }
      
           }
           return retval;
       }
    
    
    
    NoiseTerrainBuilder()
    {
   this.Noise = new SimplexNoise(1000,24.251,seed);
   geo.Regenerate(positions);
   
   
   
    }
    Mesh getMesh()
    {
     geo.Regenerate(positions);
        
  /* mesh = new Mesh();
    mesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(geo.getVertex()));
    mesh.setBuffer(VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(geo.getIndex()));
    mesh.setBuffer(VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(geo.getNormals()));
    mesh.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(geo.getText()));
    mesh.updateBound();
    mesh.setMode(Mode.Triangles);
    System.out.println(String.valueOf(positions.size()));
    //System.out.println(String.valueOf(geo.getVertex().length));
    */    
    return geo.getMesh();
    }
/*
    public Vector3f[] getVertex() {
       // return geo.getVertex();
    }

    public Vector3f[] getNormals() {
      //  return geo.getNormals();
    }

    public Vector2f[] getText() {
      //  return geo.getText();
    }

    public int[] getIndex() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   //return geo.getIndex();
           }*/

    public Vector3f[] getVertex() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Vector3f[] getNormals() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Vector2f[] getText() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int[] getIndex() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
