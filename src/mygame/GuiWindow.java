/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;
import com.simsilica.lemur.Axis;
import com.simsilica.lemur.Button;
import com.simsilica.lemur.Command;
import com.simsilica.lemur.Container;
import com.simsilica.lemur.DefaultRangedValueModel;
import com.simsilica.lemur.Label;
import com.simsilica.lemur.RangedValueModel;
import com.simsilica.lemur.Slider;
import com.simsilica.lemur.TextField;
import com.simsilica.lemur.core.VersionedReference;
import com.simsilica.lemur.text.DocumentModelFilter;
import com.simsilica.lemur.text.TextFilters;
import java.util.concurrent.Callable;

/**
 *
 * @author marko
 */

    public class GuiWindow extends Container implements Control {
           
	private static final float	SIZE_X	= 500;

	private static final float	SIZE_Y	= 500;
	

	private Label			c1;
	private Label			c2;
        private Label			c3;
        private Label			c4;
        private Label			c5;
        private Label			c6;
        private Label			c7;
	private Label				status;
	private Button				WritetoDB;
	private Button				GenerateFromDB;
        private Slider                          s0;
        private Slider                          s1;
        private Slider                          s2;
        private Slider                          s3;
        private Slider                          s4;
        private Slider                          s5;
        private Slider                          s6;
	private Slider                          s7;
        private Slider                          sa;
        private Slider                          sb;
        private Slider                          sc;
        private Slider                          soffx;
        private Slider                          soffy;
        public GuiWindow() {
		status = new Label("server status");
		WritetoDB = new Button("WritetoDB");
		GenerateFromDB = new Button("GenerateFromDB");
                DocumentModelFilter doc = new DocumentModelFilter();        
                //doc.setInputTransform();
                super.setPreferredSize(new Vector3f(600, 300, 0));
                c1 = new Label("0.1");
                c2 = new Label("0.2");
                c3 = new Label("0.3");
                c4 = new Label("0.4");
                c5 = new Label("0.5");
                c6 = new Label("0.6");
                c7 = new Label("0.6");
                //DefaultRangedValueModel RVM = new DefaultRangedValueModel();
                //RVM.setMinimum(0.000);
                //RVM.setMaximum(10.000);
                s0 = new Slider(new DefaultRangedValueModel(),Axis.X);
                s1 = new Slider(new DefaultRangedValueModel(),Axis.X);
                s2 = new Slider(new DefaultRangedValueModel(),Axis.X);
                s3 = new Slider(new DefaultRangedValueModel(),Axis.X);
                s4 = new Slider(new DefaultRangedValueModel(),Axis.X);
                s5 = new Slider(new DefaultRangedValueModel(),Axis.X);
                s6 = new Slider(new DefaultRangedValueModel(),Axis.X);
                s7 = new Slider(new DefaultRangedValueModel(),Axis.X);
                sa = new Slider(new DefaultRangedValueModel(),Axis.X);
                sb = new Slider(new DefaultRangedValueModel(),Axis.X);
                sc = new Slider(new DefaultRangedValueModel(),Axis.X);
                soffx = new Slider(new DefaultRangedValueModel(),Axis.X);
                soffy = new Slider(new DefaultRangedValueModel(),Axis.X);
                s0.getModel().setMinimum(-0.1);
                s0.getModel().setMaximum(0.1); //step
                
                s1.getModel().setMinimum(-5);
                s1.getModel().setMaximum(5);
                s2.getModel().setMinimum(-3);
                s2.getModel().setMaximum(3);
                s3.getModel().setMinimum(-2);
                s3.getModel().setMaximum(2);
                s4.getModel().setMinimum(-1);
                s4.getModel().setMaximum(1);
                s5.getModel().setMinimum(-0.5);
                s5.getModel().setMaximum(0.5);
                s6.getModel().setMinimum(-0.5);
                s6.getModel().setMaximum(0.5);
                s7.getModel().setMinimum(-1.5);
                s7.getModel().setMaximum(1.5);
                sa.getModel().setMinimum(-1);
                sa.getModel().setMaximum(1);
                sb.getModel().setMinimum(-2);
                sb.getModel().setMaximum(2);
                sc.getModel().setMinimum(-10);
                sc.getModel().setMaximum(10);
                soffx.getModel().setMinimum(-10000);
                soffx.getModel().setMaximum(10000);
                soffy.getModel().setMinimum(-10000);
                soffy.getModel().setMaximum(10000);
                //c7 = new TextField("c1");
                
                
                

		

		//username = new TextField("");
		//password = new TextField("");

		//addChild(status);
		//addChild(login);
               
		addChild(c1,0,0);
		addChild(c2,1,0);
                addChild(c3,2,0);
                addChild(c4,3,0);
                addChild(c5,4,0);
                addChild(c6,5,0);
                addChild(c7,6,0);
                addChild(sa,7,0);
                addChild(sb,8,0);
                addChild(sc,9,0);
                addChild(soffx,8,1);
                addChild(soffy,9,1);
                addChild(s0);
		addChild(GenerateFromDB);
                addChild(s1,0,1);
//                s1Index = s1.getModel().createReference();
                addChild(s2,1,1);
//                s2Index = s2.getModel().createReference();
                addChild(s3,2,1);
 //               s3Index = s3.getModel().createReference();
                addChild(s4,3,1);
//                s4Index = s4.getModel().createReference();
                addChild(s5,4,1);
 //               s5Index = s5.getModel().createReference();
                addChild(s6,5,1);
 //               s6Index = s6.getModel().createReference();
                addChild(s7,6,1);
                c1.setText("0.95");
                c2.setText("0.75");
                c3.setText("0.55");
                c4.setText("0.35");
                c5.setText("0.15");
                c6.setText("0.05");
                
                //c1.setText("name");
               
		WritetoDB.addClickCommands(new Command<Button>() {

			@Override
			public void execute(Button source) {
				
			}

		});

		GenerateFromDB.addClickCommands(new Command<Button>() {

			@Override
			public void execute(Button source) {
				
			}

		});
	}

	public double[] getData() {
            
            return new double[] {s1.getModel().getValue(),
                                 s2.getModel().getValue(),
                                 s3.getModel().getValue(),
                                 s4.getModel().getValue(),
                                 s5.getModel().getValue(),
                                 s6.getModel().getValue(),
                                 s7.getModel().getValue(),
                                 s0.getModel().getValue(),
                                 sa.getModel().getValue(),
                                 sb.getModel().getValue(),
                                 sc.getModel().getValue(),
                                 soffx.getModel().getValue(),
                                 soffy.getModel().getValue()
        };
            
                /*
                                    
                    
        */
		
	}

	public String getPassword() {
		//return password.getText();
                return null;
	}

	public void setStatus() {
                             c1.setText(String.valueOf(s1.getModel().getValue()));    
                             c2.setText(String.valueOf(s2.getModel().getValue()));    
                             c3.setText(String.valueOf(s3.getModel().getValue()));    
                             c4.setText(String.valueOf(s4.getModel().getValue()));   
                             c5.setText(String.valueOf(s5.getModel().getValue()));  
                             c6.setText(String.valueOf(s6.getModel().getValue()));
                             c7.setText(String.valueOf(s7.getModel().getValue()));
	//System.out.println(String.valueOf(s1.getModel().getPercent()));
       // System.out.println(String.valueOf(s2.getModel().getValue()));
        }

	@Override
	public Control cloneForSpatial(Spatial spatial) {
		return null;
	}

	@Override
	public void setSpatial(Spatial spatial) {
	}

	@Override
	public void update(float tpf) {
	setStatus();	
		
	}

	@Override
	public void render(RenderManager rm, ViewPort vp) {
	}

	

}

