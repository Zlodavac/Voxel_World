/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import mygame.Basic.Voxel;
import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import jme3tools.optimize.GeometryBatchFactory;

/**
 *
 * @author marko
 */
public class Cube {
    
    private ColorRGBA[][][] content;// = new ColorRGBA[10][10][10];
    private Voxel Vox = new Voxel(0.1f);
    private Node node;
    private Vector3f[] vertices = new Vector3f[1331];
    private int[] Indexes ;
    /*
    = { 100,0,200 , 
                                200,300,100 , 
                                
                                500,100,300 ,                                                         
                                300,700,500 ,
                                
                                
                                
                                2,6,3  ,
                                6,7,3 ,            
                                                
                                2,4,6 ,
                                2,0,4 ,
                                
                                4,0,1 ,
                                5,4,1 ,
                                
                                5,7,6 ,
                                5,6,4
                                 
    };
    */ 
    private Mesh mesh = new Mesh();
    public Node getNode(){
    return this.node;
}
    public Cube(AssetManager assman,Vector3f pos)
    {
        node = new Node();
       //geom = new Geometry("Voxel",Vox.GetMesh());
        for (int i = 0; i<11;i++)
        {
        for (int j = 0; j<11;j++)
        {
        for (int k = 0; k<11;k++)
        {
            vertices[Index(i,j,k)] = new Vector3f((float)i/10,(float)j/10,(float)k/10);
            
            
        /*
            Geometry geom =new Geometry("Voxel"+i+j+k,Vox.GetMesh());
       // content[i][j][k] = new ColorRGBA(ColorRGBA.randomColor());
        geom.setLocalTranslation(pos.x+i*0.1f,pos.y+j*0.1f,pos.z+k*0.1f);
        Material mat = new Material(assman,
        "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.randomColor());
        geom.setMaterial(mat);
        node.attachChild(geom);
        */
        }
        }
            
            
        }
        mesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
        SetIndex();
        mesh.setBuffer(VertexBuffer.Type.Index,    3, BufferUtils.createIntBuffer(Indexes));
        mesh.updateBound();
         Geometry geom =new Geometry("Blok",mesh);   
        geom.setLocalTranslation(0,-1,0);
        Material mat = new Material(assman,
        "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Red);
        geom.setMaterial(mat);
        node.attachChild(geom); 
        }
    public static int Index(int x,int y,int z)
    {
        //x + (y * maxX) + (z * maxX * maxY)
        int retval;
        retval = x+(y*11)+(z*11*11);
        return retval;
    }
    private void SetIndex()
    {
    ArrayList<Integer> list = new ArrayList<Integer>();
    
    list.add(Index(0,0,0));
    list.add(Index(10,0,0));
    list.add(Index(0,10,0));
    
    list.add(Index(10,0,0));
    list.add(Index(10,10,0));
    list.add(Index(10,10,0));
    
    list.add(Index(10,0,0));
    list.add(Index(10,0,10));
    list.add(Index(10,10,0));
    
    list.add(Index(10,10,0));
    list.add(Index(10,0,10));
    list.add(Index(10,10,10));
    //*/
    this.Indexes = new int[list.size()];
    for (int x= 0;x<list.size();x++)
    {
        this.Indexes[x] = list.get(x);
    }
    }
    }
    

