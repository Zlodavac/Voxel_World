/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

/**
 *
 * @author marko
 */
public class MyCubeSimple extends AbstractControl {
   
    private Node node = new Node();
    private Geometry optimizedGeometry_Opaque;
    private Geometry optimizedGeometry_Transparent;
    private boolean needsMeshUpdate;
    
    
    
    
    public MyCubeSimple()
   {
   
   }
    public MyCubeSimple(byte Msize)
   {
   
   }
    
    @Override
    public void setSpatial(Spatial spatial){
        Spatial oldSpatial = this.spatial;
        super.setSpatial(spatial);
        if(spatial instanceof Node){
            Node parentNode = (Node) spatial;
            parentNode.attachChild(node);
        }
        else if(oldSpatial instanceof Node){
            Node oldNode = (Node) oldSpatial;
            oldNode.detachChild(node);
        }
    }
    
    
    @Override
    protected void controlRender(RenderManager renderManager, ViewPort viewPort){
        
    } 
   @Override
    protected void controlUpdate(float lastTimePerFrame){
       // updateSpatial();
    }
}
