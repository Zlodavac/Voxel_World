/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import mygame.Basic.Voxel;
import mygame.Basic.VoxelPoint;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author marko
 */
public class NoiseTerrain {
   private final List<Vector3f> positions;
   private final  List<Vector2f> textures;
   private final List<Integer> index;
   private final List<Vector3f> normal;
   private final List<Vector3f> vertex; 
   private final  Voxel vl = new Voxel(0.1f);
   private final HashMap<Vector3f,VoxelPoint> map = new HashMap<Vector3f,VoxelPoint>();
   Geometry geo;
   Face f = new Face();
    NoiseTerrain (ArrayList<Vector3f> positions_)
    {
        this.positions = new ArrayList<Vector3f>();
        this.normal = new ArrayList<Vector3f>();
        this.textures = new ArrayList<Vector2f>();
        this.index = new ArrayList<Integer>();
        this.vertex = new ArrayList<Vector3f>();
        positions.clear();
        positions.addAll(positions_);
        textures.clear();
        index.clear();
        normal.clear();
        vertex.clear();
        map.clear();
    for (int i = 0; i != positions.size(); i++)
    {
       VoxelPoint vp = new VoxelPoint(vl);
       //vp.UP = true;
       map.put(positions.get(i),vp);
//       for( float j = 0;j >= positions.get(i).y; j = j+0.1f)
//       {
//           map.put(positions.get(i).add(0, j, 0), new VoxelPoint(vl));
//       }
    
    }
      rebuild();      
    }
    Mesh getMesh()
    {
        f.index.clear();
        f.normals.clear();
        f.textures.clear();
        f.vertex.clear();
    Mesh retval = new Mesh();
    Set set = map.entrySet();
    Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         Vector3f pos_ = (Vector3f) mentry.getKey();
         VoxelPoint vp = (VoxelPoint) mentry.getValue();
      f.add(vp.getFace(pos_, f.vertex.size()));
      }
    retval.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(f.getVertex()));
    retval.setBuffer(VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(f.getIndex()));
   // retval.setBuffer(VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(f.getNormals()));
    retval.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(f.getText()));
    retval.updateBound();
    retval.setMode(Mesh.Mode.Triangles);
    return retval;
    }
    

    public void Regenerate(ArrayList<Vector3f> positions_)
    {
        map.clear();
        positions.clear();
        positions.addAll(positions_);
        
        textures.clear();
        index.clear();
        normal.clear();
        vertex.clear();
        
        
    for (int i = 0; i != positions.size(); i++)
    {
       VoxelPoint vp = new VoxelPoint(vl);
      // vp.UP = true;
       map.put(positions.get(i).divide(10),vp);
      /* if (positions.get(i).y >0.1)
       {
       for( float j = 0;j >= positions.get(i).y; j = j+0.1f)
       {
           map.put(positions.get(i).add(0, j, 0), new VoxelPoint(vl));
       }
       }*/
    }
    
    rebuild();
    }

   private void rebuild() 
   {
       Set set = map.entrySet();
   Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         Vector3f pos_ = (Vector3f) mentry.getKey();
         VoxelPoint vp = (VoxelPoint) mentry.getValue();
                 
         vp.UP = !map.containsKey(pos_.add(0,1f,0));
               
         vp.DOWN = !map.containsKey(pos_.add(0,-1f,0));
         
         vp.RIGHT = !map.containsKey(pos_.add(-1f,0,0));
         
         vp.LEFT = !map.containsKey(pos_.add(1f,0,0));
       
         vp.FRONT = !map.containsKey(pos_.add(0,0,1f));
         
         vp.BACK = !map.containsKey(pos_.add(0,0,-1f));
      
         map.put(pos_, vp);
      }
   }
   public Mesh generateChunk(ArrayList<Vector3f> positions_)
    {
        
        HashMap<Vector3f,VoxelPoint> localMap = new HashMap<Vector3f,VoxelPoint>();
        List<Vector2f> txt = new ArrayList<Vector2f>();
        ArrayList<Integer> localindex = new  ArrayList<Integer>();
        ArrayList<Vector3f> localnormal = new ArrayList<Vector3f>();
        ArrayList<Vector3f> localvertex = new ArrayList<Vector3f>();
        
        localMap.clear();
        
        
    for (int i = 0; i != positions_.size(); i++)
        {
       VoxelPoint vp = new VoxelPoint(vl);
      // vp.UP = true;
       localMap.put(positions_.get(i).divide(10),vp);
      /* if (positions.get(i).y >0.1)
       {
       for( float j = 0;j >= positions.get(i).y; j = j+0.1f)
       {
           map.put(positions.get(i).add(0, j, 0), new VoxelPoint(vl));
       }
       }*/
        } 
    Set set = map.entrySet();
    Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         Vector3f pos_ = (Vector3f) mentry.getKey();
         VoxelPoint vp = (VoxelPoint) mentry.getValue();
                 
         vp.UP = !map.containsKey(pos_.add(0,1f,0));
               
         vp.DOWN = !map.containsKey(pos_.add(0,-1f,0));
         
         vp.RIGHT = !map.containsKey(pos_.add(-1f,0,0));
         
         vp.LEFT = !map.containsKey(pos_.add(1f,0,0));
       
         vp.FRONT = !map.containsKey(pos_.add(0,0,1f));
         
         vp.BACK = !map.containsKey(pos_.add(0,0,-1f));
      
         map.put(pos_, vp);
      }
      Mesh retval = new Mesh();
   
      while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         Vector3f pos_ = (Vector3f) mentry.getKey();
         VoxelPoint vp = (VoxelPoint) mentry.getValue();
      f.add(vp.getFace(pos_, f.vertex.size()));
      }
    retval.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(f.getVertex()));
    retval.setBuffer(VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(f.getIndex()));
   // retval.setBuffer(VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(f.getNormals()));
    retval.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(f.getText()));
    retval.updateBound();
    retval.setMode(Mesh.Mode.Triangles);
    return retval;
}
}