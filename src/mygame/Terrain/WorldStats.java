/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

/**
 *
 * @author marko
 */
public interface WorldStats {
   
   
    public void setWorldSize(int newsize);
    public void setWorldCoreSize(int newsize);
    public int  getWorldSize();
    public int  getWorldCoreSize();
    
    
    
}
