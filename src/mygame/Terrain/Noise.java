/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

import com.jme3.scene.Mesh;
import mygame.SimplexNoise;

/**
 *
 * @author marko
 */
public class Noise {
    
    
    private double step = 0.01;
    private final int seed = 1665453136;
    public double getNoise(double nx, double ny) { return SimplexNoise.getNoise(nx,ny); }
    public double getNoise(double nx, double ny,double nz) { return SimplexNoise.getNoise(nx,ny,nz); }
    private double e1,e2,e3,e4,e5,e6,e7,a,b,c;
    private double offsetX,offsetY;
    
    private  SimplexNoise SimplexNoise;
private static Noise instance = null;
private Noise() 
{
 this.SimplexNoise = new SimplexNoise(1000,24.251,seed);
}
public final static Noise getInstance() {
    return (instance != null) ? instance : makeInstance();
}
private static synchronized Noise makeInstance() {
    return ( instance != null ) ? instance : ( instance = new Noise() );
    
}    
    public void setParam(double... e)
     
    {
       // System.out.println(String.valueOf(e));
     //   for (int i=0 ;i<e.length; i++)
      //  {
    this.e1 = e[0];
    this.e3 = e[1];
    this.e3 = e[2];
    this.e4 = e[3];
    this.e5 = e[4];
    this.e6 = e[5];
     //System.out.println(String.valueOf(step));
    this.e7 = e[6];
    this.step = e[7];
    this.a = e[8];
    this.b = e[9];
    this.c = e[10];
    this.offsetX = e[11];
    this.offsetY = e[12];
      //  }
    
   
    }
     public double GetNoise(double x, double y) 
     { 
        
         
     return GetNoise(x,y,0.01);
     
     }
     
     public double GetNoise(double x, double y,double step) 
     { 
        
         double e;
         double nx = x*step,///(double)x), 
             ny = y*step;//(double)y),
         double d = Math.sqrt(200);
         
            e = e1 * getNoise(  nx,   ny)
              + e2 * getNoise( 2 * nx,  2 * ny) 
              + e3 * getNoise( 4 * nx,  4 * ny) 
              + e4 * getNoise( 8 * nx,  8 * ny) 
              + e5 * getNoise(16 * nx, 16 * ny) 
              + e6 * getNoise(32 * nx, 32 * ny);
     
      e = e + a - (b*Math.pow(d,c));
     
      return e;
     
     }
     public double Get3DNoiseX(double x, double y,double step) 
     { 
        
         double e;
         double nx = x*step,///(double)x), 
             ny = y*step;//(double)y),
             
         double d = Math.sqrt(200);
         
            e = e1 * getNoise( step ,nx,   ny)
              + e2 * getNoise( step ,2 * nx,  2 * ny) 
              + e3 * getNoise( step ,4 * nx,  4 * ny) 
              + e4 * getNoise( step ,8 * nx,  8 * ny) 
              + e5 * getNoise( step ,16 * nx, 16 * ny) 
              + e6 * getNoise( step ,32 * nx, 32 * ny);
     
      e = e + a - (b*Math.pow(d,c));
     return e;
     
     }
    public double Get3DNoiseY(double x, double y,double step) 
     { 
        
         double e,f,g;
        
         
         double nx = x*0.003,///(double)x), 
             ny = y*0.003;//(double)y),
              f = nx+0.2;
              g = ny+0.04;
         double d = Math.sqrt(200);
         
            e = e1 * getNoise( nx, step ,  ny)
              + e2 * getNoise( 2 * nx, step , 2 * ny) 
              + e3 * getNoise( 4 * nx, step , 4 * ny) 
              + e4 * getNoise( 8 * nx, step ,  8 * ny) 
              + e5 * getNoise( 16 * nx,step , 16 * ny) 
              + e6 * getNoise( 32 * nx,step , 32 * ny);
     
      e += e1 * getNoise( f, step ,  f)
              + e2 * getNoise( 2 * f, step , 2 * g) 
              + e3 * getNoise( 4 * f, step , 4 * g) 
              + e4 * getNoise( 8 * f, step ,  16 * g) 
              + e5 * getNoise( 16 * f,step , 32 * g) 
              + e6 * getNoise( 32 * f,step , 64 * g);
      e -= e1 * getNoise( g, step ,  f)
              + e2 * getNoise( 2 * g, step , 2 * f) 
              + e3 * getNoise( 4 * g, step , 4 * f) 
              + e4 * getNoise( 16 * g, step ,  8 * f) 
              + e5 * getNoise( 32 * g,step , 16 * f) 
              + e6 * getNoise( 46 * g,step , 32 * f);
        
       // e = e + 0.5*Math.pow(e, 1.8);
        
     return e+f+g;
     
     }
    public double Get3DNoiseZ(double x, double y,double step) 
     { 
        
         double e;
         double nx = x*step,///(double)x), 
             ny = y*step;//(double)y),
             
         double d = Math.sqrt(200);
         
            e = e1 * getNoise( nx,   ny,100 )
              + e2 * getNoise( 2 * nx,  2 * ny,step) 
              + e3 * getNoise( 4 * nx,  4 * ny,step) 
              + e4 * getNoise( 8 * nx,   8 * ny,step) 
              + e5 * getNoise( 16 * nx, 16 * ny,step) 
              + e6 * getNoise( 32 * nx, 32 * ny,step);
     
      e = e + a - (b*Math.pow(d,c));
     return e;
     
     }
}
