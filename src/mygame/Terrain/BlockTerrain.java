/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import mygame.Basic.Vector3I;
import mygame.Basic.Spiral;
/**
 *
 * @author marko
 */
public class BlockTerrain extends AbstractControl  {
    AssetManager assetManager;
    Future future = null;
   // BlockMaterial  mat;
    
    Node node = new Node();
    Vector3f position;
    int Sizex=5,Sizey=5,Sizez=5;
    HashMap<Vector3I,BlockTerrainChunk> chunks = new HashMap<Vector3I,BlockTerrainChunk>();
    boolean needupdate = true;
    public BlockTerrain (AssetManager ass)
    {
    this.assetManager = ass;
   // mat = new BlockMaterial(ass);
   for(int i = 0; i < Sizex;i++)
    {
    for(int j = 0; j < Sizey;j++)
    {
    for(int k = 0; k < Sizez;k++)
    {
    chunks.put(new Vector3I(i,j,k), 
            new BlockTerrainChunk(ass) );
     
    }
    }
    }
    Set set = chunks.entrySet();
    Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
        Map.Entry mentry = (Map.Entry)iterator.next();
         Vector3I pos = (Vector3I) mentry.getKey();
         BlockTerrainChunk chunk = (BlockTerrainChunk) mentry.getValue();
         chunk.generateRange(pos);
         node.attachChild(chunk.geom);
   
   
    }
    node.move(0, 0, 0);
    }
    
    public void regen()
    {
        needupdate = true;
    if (needupdate)
    {
    Set set = chunks.entrySet();
    Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
        Map.Entry mentry = (Map.Entry)iterator.next();
         Vector3I pos = (Vector3I) mentry.getKey();
         BlockTerrainChunk chunk = (BlockTerrainChunk) mentry.getValue();
         chunk.generateRange(pos);
         node.attachChild(chunk.geom);
      }
      needupdate = false;
    }
    }
    public void setPosition(Vector3f vec)
      {
      this.position = vec;
      }
    public void createView()
    {
        Vector3I vec = new Vector3I(this.position);
        vec.divdeLocal(8);
        vec.multLocal(8);
        //System.out.println(vec.toString()+vec.divde(8).toString());
        
        if (!chunks.containsKey(vec))
        {
            System.out.println(vec.toString());
            chunks.put(vec, new BlockTerrainChunk(this.assetManager));
            needupdate = true;
        }
        
        regen();
    }
    
    
    
    
    
    @Override
    protected void controlUpdate(float tpf) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    
    }
    @Override
    public void setSpatial(Spatial spatial){
        Spatial oldSpatial = this.spatial;
        super.setSpatial(spatial);
        if(spatial instanceof Node){
            Node parentNode = (Node) spatial;
            parentNode.attachChild(node);
        }
        else if(oldSpatial instanceof Node){
            Node oldNode = (Node) oldSpatial;
            oldNode.detachChild(node);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Spatial getSpatial()
            {
                return this.node;
}
    public void nekaj(ScheduledThreadPoolExecutor executor)
    {
    
    }

    

}
