/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain.Biomes;

import com.jme3.math.Vector3f;

/**
 *
 * @author marko
 */
public interface PlantStats {
    public float GrowRate=0.1f;
    public float ExpandRate = 0.01f;
    public int MaxNumofChildren = 4;
    public Vector3f MaxSize = new Vector3f(1f,1f,1f);
    
    
    
    
    
}
