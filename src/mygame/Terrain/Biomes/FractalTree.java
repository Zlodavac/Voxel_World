/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain.Biomes;

import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import java.util.ArrayList;
import mygame.Basic.DynamicVoxel;

/**
 *
 * @author marko
 */
public class FractalTree implements PlantStats {
    boolean needupdate = false;
    
    
    DynamicVoxel vox = new DynamicVoxel();
    
    Vector3f position = new Vector3f();
    Vector3f size = new Vector3f();
    Vector3f UP =  Vector3f.UNIT_Y;
    
    EnumPlantParts part = EnumPlantParts.SEED;
    
    boolean CanHaveChildren = true;
    
    Geometry geom = new Geometry();
    
    ArrayList<TreePart> children= new ArrayList<TreePart>();
    
    
    public FractalTree(Vector3f pos,EnumPlantParts initstatus)
    {
        this.position=pos;
        this.part = initstatus;
        vox.setSize(new Vector3f(0.1f,0.1f,0.1f));
    
    
    }
    public void grow()
    {
        switch (this.part)
        {
            case SEED:
                this.part = EnumPlantParts.SAPLING;
                needupdate = true;
                break;
                
            case SAPLING:
                this.expand();
                break;
                
            case ROOT:
                this.expand();
                break;
                
            case TRUNK:
                this.expand();
                break;
                
        
        }
    
    }
    public void expand()
    {
        
    
    }
}
