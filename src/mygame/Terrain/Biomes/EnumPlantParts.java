/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain.Biomes;

/**
 *
 * @author marko
 */
public enum EnumPlantParts {
    SEED,
    SAPLING,
    ROOT,
    TRUNK,
    BRANCHX,
    BRANCHZ,
    TWIGX,
    TWIGZ,
    LEAFS,
    FLOWERS,
    FRUIT;
      
}
