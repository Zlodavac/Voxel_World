/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain.Biomes;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.shape.Quad;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import java.util.Random;
import java.util.TreeMap;
import mygame.Basic.Voxel;
import mygame.Basic.VoxelPoint;

/**
 *
 * @author User
 */
public class Tree {
    public Vector3f pos = new Vector3f(0,0,0);
    public TreeMap<Vector3f,TreePart> treepoint
            = new TreeMap<Vector3f,TreePart>();
    public ArrayList<Geometry> body = new ArrayList<Geometry>();
    StringBuffer next = new StringBuffer();
    String current = "RF";
    AssetManager assman;
    private float size = 0.1f;
    private float maxSize = 10f;
    Random rnd = new Random();
    
    public Tree(Vector3f position, AssetManager ass)
       {
        this.assman = ass;
        this.pos = position;
      //  treepoint.put(pos, new TreePart());
      // body.add(treepart.F);
           
      // Grow();
      // Grow();
      // Grow();
      // Grow();
    
   }
    
    void Grow()
    {
    for (int i = 0; i < current.length(); i++) {
    char c = current.charAt(i);
    if (c == 'R') {
    
      next.append("FGFGFG[-BH-BH-BH]FG[+BH+BH+BH]FG[/BH/BH/BH]FG[*BH*BH*BH]R");
    } 
    else if (c == '-') {
    
      next.append("-BH-");
    }
    else if (c == '+') {
    
      next.append("+BH+");
    }
    else if (c == '/') {
    
      next.append("/BH/");
    }
    else if (c == '*') {
    
      next.append("*BH*");
    }
    else
    {
    next.append(c);
    }
    
  }
    current = next.toString();
    }
   void CreateMesh()
   {
       Vector3f position = pos;
       Vector3f Sposition = pos;
   Mesh m = new Mesh();
   Mesh mTrunk = new Mesh();
   Voxel voxTrunk = new Voxel(size);
   Voxel vox = new Voxel(size/2);
   m.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vox.getVertex()));
   m.setBuffer(VertexBuffer.Type.Index, 1, BufferUtils.createIntBuffer(vox.getIndex()));
   m.setMode(Mesh.Mode.Triangles);
   m.updateBound();
   mTrunk.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(voxTrunk.getVertex()));
   mTrunk.setBuffer(VertexBuffer.Type.Index, 1, BufferUtils.createIntBuffer(voxTrunk.getIndex()));
   mTrunk.setMode(Mesh.Mode.Triangles);
   mTrunk.updateBound();
   //m.deepClone();
   Material mat = new Material(assman, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.White);
   for (int i = 0; i < current.length(); i++) {
       char c = current.charAt(i);
       if(c=='F')
       {
           Geometry b = new Geometry("tree",mTrunk.deepClone());
           b.setMaterial(mat);
           b.move(position);
           //position = position.add(new Vector3f((float)0,(float)0.1,(float)0)) ;
           body.add(b);
       }
       else if(c=='G')
       {
           position = position.add(new Vector3f((float)0,(float)size,(float)0)) ;
       }
       else if(c=='H')
       {
           position = position.add(new Vector3f((float)0,(float)size/2f,(float)0)) ;
       }
       else if(c=='+')
       {
          //Geometry b = new Geometry("tree",m.deepClone());
          // b.setMaterial(mat);
           position = position.add(new Vector3f((float)size/2f,(float)0,(float)size/2f));
          // b.move(position);
          // body.add(b); 
       }
       else if(c=='-')
       {
       //Geometry b = new Geometry("tree",m.deepClone());
          // b.setMaterial(mat);
           position = position.add(new Vector3f((float)-size/2f,(float)0,(float)-size/2f));
          // b.move(position);
          // body.add(b); 
       }
       else if(c=='*')
       {
       //Geometry b = new Geometry("tree",m.deepClone());
          // b.setMaterial(mat);
           position = position.add(new Vector3f((float)-size/2f,(float)0,(float)size/2f));
          // b.move(position);
          // body.add(b); 
       }
       else if(c=='/')
       {
       //Geometry b = new Geometry("tree",m.deepClone());
          // b.setMaterial(mat);
           position = position.add(new Vector3f((float)size/2f,(float)0,(float)-size/2f));
          // b.move(position);
          // body.add(b); 
       }
       else if(c=='[')
       {
            Sposition = position ;
      
       }
       else if(c==']')
       {
        position = Sposition;
       
       }
       else if(c=='B')
       {
           Geometry b = new Geometry("tree",m.deepClone());
           b.setMaterial(mat);
           b.move(position);
          // position = position.add(new Vector3f((float)0,(float)0.,(float)size)) ;
           body.add(b);
       }
   }
   }
}
