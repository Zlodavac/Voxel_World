/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import mygame.Basic.Vector3I;

/**
 *
 * @author marko
 */
public class BlockWorldSurface {
    

    Future future = null;
    int size = 250;
    
  ArrayList<Vector3I> surf_Volume = new ArrayList<>();
  ArrayList<Vector3I> surf_TOP = new ArrayList<>();  
  ArrayList<Vector3I> surf_BUT = new ArrayList<>();
  ArrayList<Vector3I> surf_FRONT = new ArrayList<>();
  ArrayList<Vector3I> surf_BACK = new ArrayList<>();
  ArrayList<Vector3I> surf_LEFT = new ArrayList<>();
  ArrayList<Vector3I> surf_RIGHT = new ArrayList<>();

  public BlockWorldSurface()
  {
  }
  
  public void generate(int centerx,int centery)
  {
      surf_TOP.clear();
      surf_BUT.clear();
      surf_FRONT.clear();
      surf_BACK.clear();
      surf_LEFT.clear();
      surf_RIGHT.clear();
      surf_Volume.clear();
      
      
      
      
    for(int x = centerx-size;x<=centerx+size; x++){
    for(int y = centery-size;y<=centery+size; y++){
    surf_TOP.add(new Vector3I(x,1+(int)Math.round( Noise.getInstance().Get3DNoiseY(x, y, 0.01)),y).multLocal(10));
//    surf_BUT.add(new Vector3I(x,-size + (int)Math.round( Noise.getInstance().Get3DNoiseY(x, y, -0.01)),y));
//    
//    surf_FRONT.add(new Vector3I(x,y,size+(int)Math.round( Noise.getInstance().Get3DNoiseZ(x, y, 0.01))));
//    surf_BACK.add(new Vector3I(x,y,-size+(int)Math.round( Noise.getInstance().Get3DNoiseZ(x, y, -0.01))));
//    
//    surf_LEFT.add(new Vector3I(size+(int)Math.round( Noise.getInstance().Get3DNoiseX(x, y, 0.01)),x,y));
//    surf_RIGHT.add(new Vector3I(-size+(int)Math.round( Noise.getInstance().Get3DNoiseX(x, y, -0.01)),x,y));
//    
    }}
  
   surf_Volume.addAll(surf_TOP);
//   surf_Volume.addAll(surf_BUT);
//   surf_Volume.addAll(surf_FRONT);
//   surf_Volume.addAll(surf_BACK);
//   surf_Volume.addAll(surf_LEFT);
//   surf_Volume.addAll(surf_RIGHT);
   
  }    
  public Mesh getPoints()
  {
      Mesh m = new Mesh();
      Vector3f[] retval = new Vector3f[surf_Volume.size()];
      for(int i=0; i<surf_Volume.size();i++)
      {
          retval[i] = surf_Volume.get(i).toVector3f();
      }
      m.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(retval));
      m.setMode(Mesh.Mode.Points);
      m.updateBound();
      return m;
  }
  
}
