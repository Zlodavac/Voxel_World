/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

import com.j256.ormlite.table.DatabaseTable;
import mygame.Basic.VoxelPoint;
import mygame.Basic.Voxel;
import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.control.AbstractControl;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import mygame.Basic.Vector3I;
import mygame.BlockMaterial;
import mygame.Face;

/**
 *
 * @author marko
 */
@DatabaseTable(tableName = "chunk")
public class BlockTerrainChunk extends AbstractControl implements Callable<Boolean> {
    Future future = null;
    private final  Voxel vl = new Voxel(1f);
   // private final HashMap<Vector3I,Boolean> Existmap = new HashMap<Vector3I,Boolean>();
    private final HashMap<Vector3I,VoxelPoint> map = new HashMap<>();
    public Mesh mesh = new Mesh();
    public Node node = new Node();
    public Geometry geom = new Geometry();
    BlockMaterial mat;
    Face f = new Face();
    Vector3I center;
    int sizeX,sizeY,sizeZ;
    private boolean needupdate = false;
    
    
    public void InitChunk()
    {
     for (int x = (int)center.x - 8 ; x < (center.x + 8); x++ )
           {
           for ( int z = (int)center.z - 8 ; z < (center.z + 8); z++ )
           {
           for( int y = (int)center.y - 8 ; y < (center.y + 8); y++)
           {
         //  Existmap.put(new Vector3I(x,y,z),false);
           
           
           }}}
        
        
    }
    
    public void RefreshChunk()
    {
     for (int x = (int)center.x - 8 ; x < (center.x + 8); x++ )
           {
           for ( int z = (int)center.z - 8 ; z < (center.z + 8); z++ )
           {
           for( int y = (int)center.y - 8 ; y < (center.y + 8); y++)
           {
          // Existmap.put(new Vector3I(x,y,z),false);
           
           
           }}}
        
        
    }
    public ArrayList<Vector3I> GetAllY(int x,int z)
    {
    ArrayList<Vector3I> retval = new ArrayList<Vector3I>();
    
    
    return retval;
    }
    public BlockTerrainChunk(AssetManager ass)
            
    {
    mat = new BlockMaterial(ass);
    
    }
    public void generateRange(Vector3I center_)
       {
           center = center_.mult(8);
           //center.y = 0;
           f.clear();
            ArrayList<Vector3f> positions_ = new ArrayList<>();
           //positions.clear();
    
           
           for (int x = (int)center.x - 8 ; x < (center.x + 8); x++ )
           {
           for ( int z = (int)center.z - 8 ; z < (center.z + 8); z++ )
           {
               for( int y = (int)center.y - 8 ; y < (center.y + 8); y++)
               {
                 //  Existmap.put(new Vector3I(x,y,z), false);
               }
         double e = Noise.getInstance().GetNoise(x, z);
       
      
       e = (int)(Math.round(e*10));
            
       if( e > center.y - 8 && e < center.y + 8)     
       positions_.add(new Vector3f((float)x,(float)e,(float)z));
   
        
           }
           }
        map.clear();
      
    for (int i = 0; i != positions_.size(); i++)
        {
       VoxelPoint vp = new VoxelPoint(vl);
       Vector3I pos = new Vector3I(positions_.get(i));
       map.put(pos,vp);
       
        for(int j = pos.getY() ; j>center.y - 8;j--)
        {
       // Existmap.put(new Vector3I(pos.getX(),j,pos.getZ()), true);
        
        }
       
//      
        } 
   Set set = map.entrySet();
    Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         Vector3I pos_ = (Vector3I) mentry.getKey();
         VoxelPoint vp = (VoxelPoint) mentry.getValue();
                 
         vp.UP = !map.containsKey(pos_.add(0,1,0));
               
         vp.DOWN = !map.containsKey(pos_.add(0,-1,0));
         
         vp.RIGHT = !map.containsKey(pos_.add(-1,0,0));
         
         vp.LEFT = !map.containsKey(pos_.add(1,0,0));
       
         vp.FRONT = !map.containsKey(pos_.add(0,0,1));
         
         vp.BACK = !map.containsKey(pos_.add(0,0,-1));
      
//        
             
//        
         map.put(pos_, vp);
        
         f.add(vp.getFace(pos_.toVector3f(), f.vertex.size()));
      }
    
       mesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(f.getVertex()));
       mesh.setBuffer(VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(f.getIndex()));
            // retval.setBuffer(VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(f.getNormals()));
       mesh.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(f.getText()));
       mesh.updateBound();
       mesh.setMode(Mesh.Mode.Triangles);
     
    
       
       geom.setMesh(mesh);
       //mat.getAdditionalRenderState().setWireframe(true);
       geom.setMaterial(mat);
    

}

    public ArrayList<Vector3I> SearchAllY(Vector3I pos)
    {
    ArrayList<Vector3I> retval = new ArrayList<>();
    
    
    
    return retval;
    }
    
    @Override
    protected void controlUpdate(float tpf) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean call() throws Exception {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    
    
    return true;
    }

    
       }

    


