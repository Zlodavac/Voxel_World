/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import mygame.Basic.Vector3I;

/**
 *
 * @author marko
 */
public class RunableSurface implements Runnable {
    ArrayList<Vector3I> points = new ArrayList<>(); 
        int centerx;
        int centery;
        int size;
        Mesh m = new Mesh();
    
    
   public RunableSurface(int x,int y,int radius)
    {
             this.centerx=x;
             this.centery=y; 
             this.size=radius;
    }
    @Override
    public void run() {
        

              // TODO Auto-generated method stub

              System.out.println(Thread.currentThread().getName());

              for(int x = centerx-size;x<=centerx+size; x++){
    for(int y = centery-size;y<=centery+size; y++){
    points.add(new Vector3I(x,1+(int)Math.round( Noise.getInstance().Get3DNoiseY(x, y, 0.01)),y).multLocal(10));
    }}
             
              
      Vector3f[] retval = new Vector3f[points.size()];
      for(int i=0; i<points.size();i++)
      {
          retval[i] = points.get(i).toVector3f();
      }
      m.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(retval));
      m.setMode(Mesh.Mode.Points);
      m.updateBound();
      
     
      
      
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
