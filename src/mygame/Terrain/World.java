/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.control.AbstractControl;
import com.jme3.util.BufferUtils;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import mygame.Basic.Vector3I;
import mygame.BlockMaterial;
import mygame.Main;

/**
 *
 * @author marko
 */
public class World extends AbstractControl implements Callable<Mesh>{
 //   BlockTerrain BT = new BlockTerrain(); 
    AssetManager assman;
    Node node = new Node();
    Node nodePositions = new Node();
    Node nodeGeometry = new Node();
    mygame.Basic.Spiral spiral = new mygame.Basic.Spiral(25,25);
    private List<Point> list;// = spiral.spiral();
    int spiralPoint = 0;
    Material mat;
    Future future = null;
    HashMap<Point,WorldChunk> WC= new HashMap<>(); 
    Future futureGeom = null;
    public boolean ExpandTerrain = false;
  public World()
   {
   
   }
  public World(AssetManager ass)
   {
   this.assman = ass;
   //mat = new BlockMaterial(ass);
    mat = new Material(assman, "Common/MatDefs/Misc/Unshaded.j3md");
   list = spiral.spiral();
   node.attachChild(nodePositions);
   node.attachChild(nodeGeometry);
   }

  
  //HashMap<Integer,ArrayList<Vector3I>> GeometryMap= new HashMap();
  ArrayList<Vector3I> points = new ArrayList<>(); 
        int centerx=0;
        int centery=0;
        int size=16;
  
  private void nextPoint()
  {
      if(spiralPoint < list.size())
      {
  Vector3I point = new Vector3I(list.get(spiralPoint).x,0,list.get(spiralPoint).y);
  point.multLocal(2*size);
  centerx = point.x;
  centery = point.z;
  spiralPoint++;
  }
      else ExpandTerrain = false;
      
  }
  
    

    @Override
    protected void controlUpdate(float tpf) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    try{
        //If we have no waylist and not started a callable yet, do so!
        if( this.ExpandTerrain == true && future == null && futureGeom==null){
            //set the desired location vector, after that we should not modify it anymore
            //because it's being accessed on the other thread!
            //desiredLocation.set(getGoodNextLocation());
            //start the callable on the executor
           // future = executor.submit(findWay);    //  Thread starts!
           future = Main.executor.submit(this);
           WC.put(list.get(spiralPoint), new WorldChunk(list.get(spiralPoint).x,
                                                        list.get(spiralPoint).y,
                                                           size              ));
           futureGeom = Main.executor.submit((WorldChunk)WC.get(list.get(spiralPoint)));
        this.nextPoint();
        }
        //If we have started a callable already, we check the status
        else if(future != null){
            //Get the waylist when its done
            if(future.isDone()){
               
                Mesh m = (Mesh) future.get();
                Geometry g = new Geometry(String.valueOf(spiralPoint),m); 
                
                g.setMaterial(mat);
                this.nodePositions.attachChild(g);
               
                
                future = null;
                 System.out.println(this.node.getQuantity());
            }
            else if(future.isCancelled()){
                //Set future to null. Maybe we succeed next time...
                future = null;
            }
        }
        else if(futureGeom !=null)
            {
                 if(futureGeom.isDone()){
               
                Mesh m = (Mesh) futureGeom.get();
                Geometry g = new Geometry(String.valueOf(spiralPoint),m); 
                
                g.setMaterial(mat);
                this.nodeGeometry.attachChild(g);
               
                
                futureGeom = null;
                 System.out.println(this.node.getQuantity());
            }
            else if(futureGeom.isCancelled()){
                //Set future to null. Maybe we succeed next time...
                futureGeom = null;
            }
        }
        }
        catch(Exception e){
      System.out.println(e.getMessage());
    }
    
    
    
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Mesh call() throws Exception {
    
        System.out.println(Thread.currentThread().getName());
    for(int x = centerx-size;x<=centerx+size; x++){
    for(int y = centery-size;y<=centery+size; y++){
    points.add(new Vector3I(x,(int)Math.round( Noise.getInstance().Get3DNoiseY(x, y, 0.01)*10),y).multLocal(10));
    }}
              Mesh m = new Mesh();
              
      Vector3f[] retval = new Vector3f[points.size()];
      for(int i=0; i<points.size();i++)
      {
          retval[i] = points.get(i).toVector3f();
      }
      
      m.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(retval));
      m.setMode(Mesh.Mode.Points);
      m.updateBound();
     // GeometryMap.put(spiralPoint, points);
      
      
     // points = null;
     System.out.println(centerx+" "+centery);
    
           
     points.clear();
     //nextPoint();
      return m.clone();//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    }
  @Override
    public Spatial getSpatial()
            {
                return this.node;
}
     public void clear()
     {
         this.nodeGeometry.detachAllChildren();
     this.nodePositions.detachAllChildren();
     this.spiralPoint = 0;
     this.WC.clear();
     }
     
     @Override
    public void setSpatial(Spatial spatial){
        Spatial oldSpatial = this.spatial;
        super.setSpatial(spatial);
        if(spatial instanceof Node){
            Node parentNode = (Node) spatial;
            parentNode.attachChild(node);
        }
        else if(oldSpatial instanceof Node){
            Node oldNode = (Node) oldSpatial;
            oldNode.detachChild(node);
        }
    }
     
     
     
}
