/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Terrain;

import com.jme3.scene.Mesh;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import mygame.Basic.Vector3I;
import mygame.Basic.Voxel;
import mygame.Basic.VoxelPoint;
import mygame.Face;

/**
 *
 * @author marko
 */
public class WorldChunk implements Callable<Mesh> {

    private Face f = new Face();
    private final  VoxelPoint vl = new VoxelPoint(new Voxel(10f));
    private ArrayList<Vector3I> points=new ArrayList<>();
    int centerx,centery,size;
    Mesh mesh = new Mesh();
    
    public WorldChunk(int x,int y,int radius)
    {
    centerx = x*2*radius;
    centery = y*2*radius;
    size = radius;
      
    }
    
    @Override
    public Mesh call() throws Exception {
        for(int x = centerx-size;x<=centerx+size; x++){
        for(int y = centery-size;y<=centery+size; y++){
        points.add(new Vector3I(x,(int)Math.round( Noise.getInstance().Get3DNoiseY(x, y, 0.01)*10),y).multLocal(10));
        }}
        System.out.println(points.size());
        vl.UP = true;
        vl.BACK = true;
        vl.RIGHT = true;
        points.forEach((point) -> {
            f.add(vl.getFace(point.toVector3f(), f.vertex.size()));
        });
        mesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(f.getVertex()));
       mesh.setBuffer(VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(f.getIndex()));
            // retval.setBuffer(VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(f.getNormals()));
       mesh.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(f.getText()));
       mesh.updateBound();
       mesh.setMode(Mesh.Mode.Triangles);
       return mesh;
        

//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
    
    
    
}
