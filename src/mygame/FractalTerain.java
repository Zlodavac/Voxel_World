/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class FractalTerain extends AbstractControl {
private final Node node= new Node();
NoiseTerrainBuilder teren = new NoiseTerrainBuilder();
private final AssetManager assetManager;
       BlockMaterial mat;
private boolean needUpdate = true;
public FractalTerain(AssetManager assetManager_)
 
{
 this.assetManager = assetManager_;
 mat = new BlockMaterial(assetManager);
 this.node.move(0, -10, 10);
}
Geometry geo = new Geometry();
  public void setParam(double... e)
  {
  teren.setParam(e);
  needUpdate = true;
  this.build();
  }
    
    public void build()
    {
        if (needUpdate)
        {
        if (geo.getMesh() == null)
        {
    teren.generate(100, 100);
    geo.setMesh(teren.getMesh());
    geo.setMaterial(mat);
    this.node.attachChild(geo);
        }
        else
        {
           
            teren.generate(50, 50);
            geo.setMesh(teren.getMesh());
           // geo.getMesh().scaleTextureCoordinates(new Vector2f(100,100));
            geo.setMaterial(mat);
            this.node.detachAllChildren();
            this.node.attachChild(geo);
            

    
        
    
    
        }
        needUpdate = false;
        }
        
    }
    
    
    
    @Override
    protected void controlUpdate(float tpf) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void setSpatial(Spatial spatial){
        Spatial oldSpatial = this.spatial;
        super.setSpatial(spatial);
        if(spatial instanceof Node){
            Node parentNode = (Node) spatial;
            parentNode.attachChild(node);
        }
        else if(oldSpatial instanceof Node){
            Node oldNode = (Node) oldSpatial;
            oldNode.detachChild(node);
        }
    }
    
  
}
