/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.AI;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.Bone;
import com.jme3.animation.LoopMode;
import com.jme3.animation.Skeleton;
import com.jme3.animation.SkeletonControl;
import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Box;

/**
 *
 * @author User
 */
public class AIPlayer extends AbstractControl {

    Node node = new Node();
  public  AIPlayer(AssetManager assman){
     Box torso = new Box(1f,2f,0.5f);
     Box head = new Box (0.2f,0.2f,0.2f);
     Box Lleg = new Box(0.3f,1f,0.3f);
     Box Rleg = new Box(0.3f,1f,0.3f);
     
     
     Geometry TorsoGeom = new Geometry("torso", torso);
     Geometry HeadGeom = new Geometry("head", head);
     Geometry LLGeom = new Geometry("Lleg", Lleg);
     Geometry RLGeom = new Geometry("Rleg", Rleg);
     Material mat = new Material(assman,"Common/MatDefs/Misc/Unshaded.j3md" );
     mat.setColor("Color", ColorRGBA.Blue);
     TorsoGeom.move(0, 0, 0);
     HeadGeom.move(0, 2f, 0);
     LLGeom.move(0.8f, -1, 0);
     RLGeom.move(-0.8f, -1, 0);
        
     node.attachChild(TorsoGeom);
     node.attachChild(HeadGeom);
     node.attachChild(LLGeom);
     node.attachChild(RLGeom);
     node.setMaterial(mat);
 
     Bone SpineBone = new Bone("Spine");
     Bone HeadBone = new Bone("Head");
     Bone LLBone = new Bone("LeftLeg");
     Bone RLBone = new Bone("RightLeg");
     Bone[] bones = {SpineBone, HeadBone, LLBone, RLBone};
     
     HeadBone.setBindTransforms(new Vector3f(0,1,0), Quaternion.IDENTITY, Vector3f.UNIT_XYZ);
     LLBone.setBindTransforms(new Vector3f(1,0,0), Quaternion.IDENTITY, Vector3f.UNIT_XYZ);
     RLBone.setBindTransforms(new Vector3f(-1,0,0), Quaternion.IDENTITY, Vector3f.UNIT_XYZ);
     SpineBone.addChild(HeadBone);
     SpineBone.addChild(LLBone);
     SpineBone.addChild(RLBone);
     
     Skeleton skel = new Skeleton(bones);
     SkeletonControl control = new SkeletonControl(skel);
     node.addControl(control);
     
     
     AnimControl playerControl = new AnimControl();
     node.addControl(playerControl);
     AnimChannel channel_walk = playerControl.createChannel();
     channel_walk.addAllBones();
     channel_walk.setLoopMode(LoopMode.Loop);
     
    }
    @Override
    protected void controlUpdate(float tpf) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void setSpatial(Spatial spatial){
        Spatial oldSpatial = this.spatial;
        super.setSpatial(spatial);
        if(spatial instanceof Node){
            Node parentNode = (Node) spatial;
            parentNode.attachChild(node);
        }
        else if(oldSpatial instanceof Node){
            Node oldNode = (Node) oldSpatial;
            oldNode.detachChild(node);
        }
    }
    @Override
    public Spatial getSpatial()
            {
                return this.node;
}
}
