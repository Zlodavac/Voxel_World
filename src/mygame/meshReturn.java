/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

/**
 *
 * @author marko
 */
public interface meshReturn {
    public Vector3f[] getVertex();
    public Vector3f[] getNormals();
    public Vector2f[] getText();
    public int[] getIndex();
}
