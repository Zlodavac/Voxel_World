/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import java.util.ArrayList;

/**
 *
 * @author marko
 */
public class Face implements meshReturn{
  public ArrayList<Vector3f> vertex = new ArrayList<Vector3f>();
  public ArrayList<Vector3f> normals= new ArrayList<Vector3f>();
  public ArrayList<Integer> index= new ArrayList<Integer>();
  public ArrayList<Vector2f> textures= new ArrayList<Vector2f>();
   
  public void clear()
  {
  vertex.clear();
  normals.clear();
  index.clear();
  textures.clear();
  
  }
  
  public void add(Face f)
  {
  vertex.addAll(f.vertex);
  normals.addAll(f.normals);
  index.addAll(f.index);
  textures.addAll(f.textures);
  
  }

    public Vector3f[] getVertex() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    return this.vertex.toArray(new Vector3f[0]);
    
    }

    public Vector3f[] getNormals() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Vector2f[] getText() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     return this.textures.toArray(new Vector2f[0]);
    }

    public int[] getIndex() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
   int[] retval  = new int[this.index.size()];
    for (int i = 0 ;i<index.size();i++)
    {
        retval[i] = index.get(i);
    }
   
   return retval;
    }
  public void divide(int scale)
  {
      for (int i = 0; i< vertex.size();i++)
        this.vertex.set(i, new Vector3f(this.vertex.get(i).divide((float)scale)));
  
  }
}
