/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Basic;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Voxel {
   ArrayList<Vector3f> vertex = new ArrayList<Vector3f>();
   ArrayList<Integer> index = new ArrayList<Integer>();
   ArrayList<Vector2f> TextCoord = new ArrayList<Vector2f>();
   ArrayList<Vector3f> normal = new ArrayList<Vector3f>();
   float size;
   Vector3f Txtsize =  Vector3f.NAN;
public Voxel(float size_)
{
    size = size_/0x2;
vertex.add(new Vector3f((float) -size, (float)-size,(float)-size));//0
vertex.add(new Vector3f((float)-size,(float)-size,(float)size));//1
vertex.add(new Vector3f((float)-size,(float)size,(float)-size));//2
vertex.add(new Vector3f((float)-size,(float)size,(float)size));//3

vertex.add(new Vector3f((float)size,(float)size,(float)-size));//4
vertex.add(new Vector3f((float)size,(float)size,(float)size));//5
vertex.add(new Vector3f((float)size,(float)-size,(float)-size));//6
vertex.add(new Vector3f((float)size,(float)-size,(float)size));//7


vertex.add(new Vector3f((float)-size,(float)size,(float)size));//8 3
vertex.add(new Vector3f((float)size,(float)size,(float)size));//9 5
vertex.add(new Vector3f((float)-size,(float)-size,(float)size));//10 1
vertex.add(new Vector3f((float)size,(float)-size,(float)size));//11 7

vertex.add(new Vector3f((float) -size, (float)-size,(float)-size));;//12 0
vertex.add(new Vector3f((float)size,(float)-size,(float)-size));//13 6
vertex.add(new Vector3f((float)-size,(float)size,(float)-size));//14 2
vertex.add(new Vector3f((float)size,(float)size,(float)-size));//15 4
////

index.add(0);
index.add(1); //1
index.add(2);

index.add(3);
index.add(2); //2
index.add(1);

index.add(2);
index.add(3); //3
index.add(4);

index.add(3);
index.add(5); //4
index.add(4);

index.add(5);
index.add(6); //5
index.add(4);

index.add(6);
index.add(5); //6
index.add(7);

index.add(8);
index.add(11); //7
index.add(9);

index.add(8);
index.add(10); //8
index.add(11);

index.add(10);
index.add(12); //9
index.add(13);

index.add(10);
index.add(13); //10
index.add(11);

index.add(12);
index.add(14); //11
index.add(13);

index.add(13);
index.add(14); //12
index.add(15);

this.createNormals();
this.createTxtCoord();
}
public void setTxTSize(Vector3f size)
{
this.Txtsize = size;

}

public int[] getIndex()
{
    int[] retval = new int[this.index.size()];
    for(int i =0;i!= index.size();i++)
    {
    retval[i] = index.get(i);
    }
    return retval;
}
public Vector3f[] getVertex()
{
return this.vertex.toArray(new Vector3f[vertex.size()]);
}
public Vector3f[] getNormals()
{
return this.normal.toArray(new Vector3f[normal.size()]);
}
public Vector2f[] getText()
{
return this.TextCoord.toArray(new Vector2f[this.TextCoord.size()]);
}
public ArrayList<Vector3f> GetPositiveXVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add(new Vector3f((float)size,(float)-size,(float)-size).add(offset));
    retval.add(new Vector3f((float)size,(float)-size,(float)size).add(offset));//1
    retval.add(new Vector3f((float)size,(float)size,(float)-size).add(offset));//2
    retval.add(new Vector3f((float)size,(float)size,(float)size).add(offset));

    return retval;
}

public ArrayList<Vector3f> GetNegativeXVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add(new Vector3f((float)-size,(float)-size,(float)-size).add(offset));
    retval.add(new Vector3f((float)-size,(float)size,(float)-size).add(offset));//2
    retval.add(new Vector3f((float)-size,(float)-size,(float)size).add(offset));//1

    retval.add(new Vector3f((float)-size,(float)size,(float)size).add(offset));
return retval;
}
public ArrayList<Vector3f> GetPositiveZVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add(new Vector3f((float)-size,(float)-size,(float)size).add(offset));
    retval.add(new Vector3f((float)-size,(float)size,(float)size).add(offset));//2
    retval.add(new Vector3f((float)size,(float)-size,(float)size).add(offset));//1
    retval.add(new Vector3f((float)size,(float)size,(float)size).add(offset));
return retval;
}

public ArrayList<Vector3f> GetNegativeYVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();
   retval.add( new Vector3f((float)-size,(float)-size,(float)-size).add(offset));//0
   retval.add(new Vector3f((float)-size,(float)-size,(float)size).add(offset));//2
   retval.add( new Vector3f((float)size,(float)-size,(float)-size).add(offset));//1
   retval.add(new Vector3f((float)size,(float)-size,(float)size).add(offset));
return retval;
}

public ArrayList<Vector3f> GetPositiveYVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add( new Vector3f((float)-size,(float)size,(float)-size).add(offset));
    retval.add(new Vector3f((float)size,(float)size,(float)-size).add(offset));//1
    retval.add(new Vector3f((float)-size,(float)size,(float)size).add(offset));//2
    retval.add(new Vector3f((float)size,(float)size,(float)size).add(offset));
return retval;
}

public ArrayList<Vector3f> GetNegativeZVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

      retval.add(new Vector3f((float)-size,(float)-size,(float)-size).add(offset));

        retval.add(new Vector3f((float)size,(float)-size,(float)-size).add(offset));//1
        retval.add(new Vector3f((float)-size,(float)size,(float)-size).add(offset));//2
        retval.add(new Vector3f((float)size,(float)size,(float)-size).add(offset));
return retval;
}
public ArrayList<Integer> GetFaceIndex(int offset)
{
  ArrayList<Integer> retval = new ArrayList<Integer>();  
//int[] retval={1+offset,0+offset,2+offset,1+offset,2+offset,3+offset};
retval.add(1+offset);
retval.add(0+offset);
retval.add(2+offset);
retval.add(1+offset);
retval.add(2+offset);
retval.add(3+offset);
  
  
  return retval;
}
public ArrayList<Vector2f> getFaceText()
{
 ArrayList<Vector2f> retval = new ArrayList<Vector2f>();  
retval.add(new Vector2f(1,0f));
retval.add(new Vector2f(0.5f,0f));

retval.add(new Vector2f(1f,0.3f));
retval.add(new Vector2f(0.5f,0.3f));

return retval;
}
public void createTxtCoord()
{   
this.TextCoord.add(new Vector2f(1,0f));
this.TextCoord.add(new Vector2f(0.5f,0f));

this.TextCoord.add(new Vector2f(1f,0.3f));
this.TextCoord.add(new Vector2f(0.5f,0.3f));

this.TextCoord.add(new Vector2f(1,0.6f));
this.TextCoord.add(new Vector2f(0.5f,0.6f));

this.TextCoord.add(new Vector2f(1,1));
this.TextCoord.add(new Vector2f(0.5f,1));
//
this.TextCoord.add(new Vector2f(0.5f,0f));
this.TextCoord.add(new Vector2f(0f,0f));

this.TextCoord.add(new Vector2f(0.5f,0.3f));
this.TextCoord.add(new Vector2f(0.f,0.3f));

this.TextCoord.add(new Vector2f(0.5f,0.6f));
this.TextCoord.add(new Vector2f(0.f,0.6f));

this.TextCoord.add(new Vector2f(0.5f,1));
this.TextCoord.add(new Vector2f(0.f,1));

}
public void createNormals()
{
for (Vector3f w: this.vertex)
{
this.normal.add(w.negate());
}
}
public ArrayList<Integer> getIndexList(int offset)
{
    ArrayList<Integer> retval= new ArrayList<Integer>();
    for (int i=0; i != index.size(); i++)
    {
    retval.add(index.get(i) + offset);
    }
    
    
return retval;
}
public ArrayList<Vector3f> getVertexList(Vector3f offset)
{
    ArrayList<Vector3f> retval= new ArrayList<Vector3f>();
    for (int i=0; i != vertex.size(); i++)
    {
    retval.add(vertex.get(i).add(offset));
    
    }
    
    
return retval;
}
public ArrayList<Vector3f> getNormalsList(Vector3f offset)
{
    ArrayList<Vector3f> retval= new ArrayList<Vector3f>();
    for ( Vector3f e:this.vertex)
    {
    retval.add(e.negate());
    }
    
    
return retval;
}

public ArrayList<Vector2f> getTextList(Vector3f offset)
{
return this.TextCoord;
}




}
