/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Basic;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import java.util.ArrayList;
import mygame.Face;

/**
 *
 * @author marko
 */
public class VoxelPoint  { 
     
   public  boolean UP = false;
   public  boolean DOWN = false;
   public  boolean FRONT = false;
   public  boolean BACK = false;
   public  boolean LEFT = false;
   public  boolean RIGHT = false;
   public  Voxel v;
     
  public   VoxelPoint(Voxel vox,boolean... faces)
     {
   
     v = vox;
     if (faces.length == 6)
     {
     UP = faces[0];
     DOWN = faces[1];
     FRONT = faces[2];
     BACK = faces[3];
     LEFT = faces[4];
     RIGHT = faces[5];
     }
     }
    public Face getFace(Vector3f position,int index) {
    
       //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    int i = index;
    
    Face retval = new Face();
       if(UP)
    {
    retval.vertex.addAll(v.GetPositiveYVertex(position));
    retval.index.addAll(v.GetFaceIndex(i));
    retval.textures.addAll(v.getFaceText());
    i = index+ retval.vertex.size();
    }
    if(DOWN)
    {
    retval.vertex.addAll(v.GetNegativeYVertex(position));
    retval.index.addAll(v.GetFaceIndex(i));
    retval.textures.addAll(v.getFaceText());
    i =index+ retval.vertex.size();
    }
    if(FRONT)
    {
    retval.vertex.addAll(v.GetPositiveZVertex(position));
    retval.index.addAll(v.GetFaceIndex(i));
    retval.textures.addAll(v.getFaceText());
    i =index+ retval.vertex.size();
    }
    if(BACK)
    {
    retval.vertex.addAll(v.GetNegativeZVertex(position));
    retval.index.addAll(v.GetFaceIndex(i));
    retval.textures.addAll(v.getFaceText());
    i =index+ retval.vertex.size();
    }
    if(LEFT)
    {
    retval.vertex.addAll(v.GetPositiveXVertex(position));
    retval.index.addAll(v.GetFaceIndex(i));
    retval.textures.addAll(v.getFaceText());
    i =index + retval.vertex.size();
    }
    
    if(RIGHT)
    {
    retval.vertex.addAll(v.GetNegativeXVertex(position));
    retval.index.addAll(v.GetFaceIndex(i));
    retval.textures.addAll(v.getFaceText());
    i =index + retval.vertex.size();
    }
    return retval;
    }
}

