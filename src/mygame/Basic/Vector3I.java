/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Basic;

import com.jme3.math.Vector3f;

/**
 *
 * @author marko
 */

 public final class Vector3I {
     public String ToLocalDBStr()
     {
     return "X"+this.x+"Y"+this.y+"Z"+this.z;
     }
     public Vector3I(Vector3f vec)
     {
        this();
        this.x = (int) vec.x;
        this.y = (int) vec.y;
        this.z = (int) vec.z;
    }
     public Vector3I(Vector3I vec)
     {
        this();
        this.x = (int) vec.x;
        this.y = (int) vec.y;
        this.z = (int) vec.z;
    }
    public Vector3I(int x, int y, int z){
        this();
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public Vector3I GetGridValue()
    {
        
    return this.divde(8f);
    }
    public  Vector3I divdeLocal(float e)
    {
    this.x = (int) (this.x/e);
    this.y = (int) (this.y/e);
    this.z = (int) (this.z/e);
    return this;
    }
    public Vector3I divde(float e)
    {
    
   return new Vector3I ((int) (this.x/e),(int) (this.y/e),(int) (this.z/e));
    }

    public Vector3I(){
        
    }
    public Vector3I(int s){
        this.x=s;
        this.y=s;
        this.z=s;
    }
    public int x;
    public int y;
    public int z;

    public int getX(){
        return x;
    }

    public Vector3I setX(int x){
        this.x = x;
        return this;
    }

    public int getY(){
        return y;
    }

    public Vector3I setY(int y){
        this.y = y;
        return this;
    }

    public int getZ(){
        return z;
    }

    public Vector3I setZ(int z){
        this.z = z;
        return this;
    }
    
    public boolean hasNegativeCoordinate(){
        return ((x < 0) || (y < 0) || (z < 0));
    }
    
    public Vector3I set(Vector3I vector3Int){
        return set(vector3Int.getX(), vector3Int.getY(), vector3Int.getZ());
    }

    public Vector3I set(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }
    
    public Vector3I add(Vector3I vector3Int){
        return add(vector3Int.getX(), vector3Int.getY(), vector3Int.getZ());
    }
    
    public Vector3I add(int x, int y, int z){
        return new Vector3I(this.x + x, this.y + y, this.z + z);
    }
    
    public Vector3I addLocal(Vector3I vector3Int){
       return addLocal(vector3Int.getX(), vector3Int.getY(), vector3Int.getZ());
    }
    
    public Vector3I addLocal(int x, int y, int z){
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }
    
    public Vector3I subtract(Vector3I vector3Int){
        return subtract(vector3Int.getX(), vector3Int.getY(), vector3Int.getZ());
    }
    
    public Vector3I subtract(int x, int y, int z){
        return new Vector3I(this.x - x, this.y - y, this.z - z);
    }
    
    public Vector3I subtractLocal(Vector3I vector3Int){
        return subtractLocal(vector3Int.getX(), vector3Int.getY(), vector3Int.getZ());
    }
    
    public Vector3I subtractLocal(int x, int y, int z){
        this.x -= x;
        this.y -= y;
        this.z -= z;
        return this;
    }
    public boolean IsOnY(int x_, int z_)
    {
         return this.x == x_ && this.z == z_;
            
    }
    public boolean IsOnZ(int x_, int y_)
    {
         return this.x == x_ && this.z == y_;
            
    }
    public boolean IsOnX(int y_, int z_)
    {
         return this.x == y_ && this.z == z_;
            
    }
    public Vector3I negate(){
        return mult(-1);
    }
    
    public Vector3I mult(int factor){
        return mult(factor, factor, factor);
    }
    
    public Vector3I mult(int x, int y, int z){
        return new Vector3I(this.x * x, this.y * y, this.z * z);
    }
    
    public Vector3I negateLocal(){
        return multLocal(-1);
    }
    
    public Vector3I multLocal(int factor){
        return multLocal(factor, factor, factor);
    }
    
    public Vector3I multLocal(int x, int y, int z){
        this.x *= x;
        this.y *= y;
        this.z *= z;
        return this;
    }
    
    @Override
    public Vector3I clone(){
        return new Vector3I(x, y, z);
    }

    @Override
    public boolean equals(Object object){
        if(object instanceof Vector3I){
            Vector3I vector3Int = (Vector3I) object;
            return ((x == vector3Int.getX()) && (y == vector3Int.getY()) && (z == vector3Int.getZ()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + this.x;
        hash = 79 * hash + this.y;
        hash = 79 * hash + this.z;
        return hash;
    }

    @Override
    public String toString(){
        return "[Vector3Int x=" + x + " y=" + y + " z=" + z + "]";
    }
    public Vector3f toVector3f()
    {
    return new Vector3f(this.x,this.y,this.z);
           
    }

    public int compareTo(Vector3I o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}   

