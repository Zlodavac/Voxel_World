/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.Basic;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;
import java.util.ArrayList;
import mygame.Face;

/**
 *
 * @author marko
 */
public class DynamicVoxel {
    
   ArrayList<Vector3f> vertex = new ArrayList<Vector3f>();
   ArrayList<Integer> index = new ArrayList<Integer>();
   ArrayList<Vector2f> TextCoord = new ArrayList<Vector2f>();
   ArrayList<Vector3f> normal = new ArrayList<Vector3f>();
    public  boolean UP = true;
    public  boolean DOWN = true;
    public  boolean FRONT = true;
    public  boolean BACK = true;
    public  boolean LEFT = true;
    public  boolean RIGHT = true;
    float sizeX=0.5f,sizeY=0.5f,sizeZ=0.5f;
    Geometry geom;
    Mesh mesh = new Mesh();
    Vector3f position = new Vector3f();
    Face f = new Face();
    public DynamicVoxel()
    {
        
    
    }
    public DynamicVoxel(int sizex,int sizey,int sizez)
    {
     sizeX = sizex;
     sizeY = sizey;
     sizeZ = sizez;
    }
    public DynamicVoxel(Vector3f size)
    {
       sizeX = size.x;
       sizeY = size.y;
       sizeZ = size.z; 
    
    }
    void setVertex()
    {
vertex.add(new Vector3f((float) -sizeX, (float)-sizeY,(float)-sizeZ));//0
vertex.add(new Vector3f((float)-sizeX,(float)-sizeY,(float)sizeZ));//1
vertex.add(new Vector3f((float)-sizeX,(float)sizeY,(float)-sizeZ));//2
vertex.add(new Vector3f((float)-sizeX,(float)sizeY,(float)sizeZ));//3

vertex.add(new Vector3f((float)sizeX,(float)sizeY,(float)-sizeZ));//4
vertex.add(new Vector3f((float)sizeX,(float)sizeY,(float)sizeZ));//5
vertex.add(new Vector3f((float)sizeX,(float)-sizeY,(float)-sizeZ));//6
vertex.add(new Vector3f((float)sizeX,(float)-sizeY,(float)sizeZ));//7


vertex.add(new Vector3f((float)-sizeX,(float)sizeY,(float)sizeZ));//8 3
vertex.add(new Vector3f((float)sizeX,(float)sizeY,(float)sizeZ));//9 5
vertex.add(new Vector3f((float)-sizeX,(float)-sizeY,(float)sizeZ));//10 1
vertex.add(new Vector3f((float)sizeX,(float)-sizeY,(float)sizeZ));//11 7

vertex.add(new Vector3f((float) -sizeX, (float)-sizeY,(float)-sizeZ));;//12 0
vertex.add(new Vector3f((float)sizeX,(float)-sizeY,(float)-sizeZ));//13 6
vertex.add(new Vector3f((float)-sizeX,(float)sizeY,(float)-sizeZ));//14 2
vertex.add(new Vector3f((float)sizeX,(float)sizeY,(float)-sizeZ));//15 4 
    }
    void setindex()
    {
    index.add(0);
    index.add(1); //1
    index.add(2);

    index.add(3);
    index.add(2); //2
    index.add(1);

    index.add(2);
    index.add(3); //3
    index.add(4);

    index.add(3);
    index.add(5); //4
    index.add(4);

    index.add(5);
    index.add(6); //5
    index.add(4);
    
    index.add(6);
    index.add(5); //6
    index.add(7);

    index.add(8);
    index.add(11); //7
    index.add(9);

    index.add(8);
    index.add(10); //8
    index.add(11);

    index.add(10);
    index.add(12); //9
    index.add(13);

    index.add(10);
    index.add(13); //10
    index.add(11);

    index.add(12);
    index.add(14); //11
    index.add(13);

    index.add(13);
    index.add(14); //12
    index.add(15);
    
    }
    
   public ArrayList<Vector3f> GetPositiveXVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add(new Vector3f((float)sizeX,(float)-sizeY,(float)-sizeZ).add(offset));
    retval.add(new Vector3f((float)sizeX,(float)-sizeY,(float)sizeZ).add(offset));//1
    retval.add(new Vector3f((float)sizeX,(float)sizeY,(float)-sizeZ).add(offset));//2
    retval.add(new Vector3f((float)sizeX,(float)sizeY,(float)sizeZ).add(offset));

    return retval;
}

public ArrayList<Vector3f> GetNegativeXVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add(new Vector3f((float)-sizeX,(float)-sizeY,(float)-sizeZ).add(offset));
    retval.add(new Vector3f((float)-sizeX,(float)sizeY,(float)-sizeZ).add(offset));//2
    retval.add(new Vector3f((float)-sizeX,(float)-sizeY,(float)sizeZ).add(offset));//1

    retval.add(new Vector3f((float)-sizeX,(float)sizeY,(float)sizeZ).add(offset));
return retval;
}
public ArrayList<Vector3f> GetPositiveZVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add(new Vector3f((float)-sizeX,(float)-sizeY,(float)sizeZ).add(offset));
    retval.add(new Vector3f((float)-sizeX,(float)sizeY,(float)sizeZ).add(offset));//2
    retval.add(new Vector3f((float)sizeX,(float)-sizeY,(float)sizeZ).add(offset));//1
    retval.add(new Vector3f((float)sizeX,(float)sizeY,(float)sizeZ).add(offset));
return retval;
}

public ArrayList<Vector3f> GetNegativeYVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();
   retval.add( new Vector3f((float)-sizeX,(float)-sizeY,(float)-sizeZ).add(offset));//0
   retval.add(new Vector3f((float)-sizeX,(float)-sizeY,(float)sizeZ).add(offset));//2
   retval.add( new Vector3f((float)sizeX,(float)-sizeY,(float)-sizeZ).add(offset));//1
   retval.add(new Vector3f((float)sizeX,(float)-sizeY,(float)sizeZ).add(offset));
return retval;
}

public ArrayList<Vector3f> GetPositiveYVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

    retval.add( new Vector3f((float)-sizeX,(float)sizeY,(float)-sizeZ).add(offset));
    retval.add(new Vector3f((float)sizeX,(float)sizeY,(float)-sizeZ).add(offset));//1
    retval.add(new Vector3f((float)-sizeX,(float)sizeY,(float)sizeZ).add(offset));//2
    retval.add(new Vector3f((float)sizeX,(float)sizeY,(float)sizeZ).add(offset));
return retval;
}

public ArrayList<Vector3f> GetNegativeZVertex(Vector3f offset)
{
ArrayList<Vector3f> retval= new ArrayList<Vector3f>();

      retval.add(new Vector3f((float)-sizeX,(float)-sizeY,(float)-sizeZ).add(offset));

        retval.add(new Vector3f((float)sizeX,(float)-sizeY,(float)-sizeZ).add(offset));//1
        retval.add(new Vector3f((float)-sizeX,(float)sizeY,(float)-sizeZ).add(offset));//2
        retval.add(new Vector3f((float)sizeX,(float)sizeY,(float)-sizeZ).add(offset));
return retval;
}
public ArrayList<Integer> GetFaceIndex(int offset)
{
  ArrayList<Integer> retval = new ArrayList<Integer>();  
//int[] retval={1+offset,0+offset,2+offset,1+offset,2+offset,3+offset};
retval.add(1+offset);
retval.add(0+offset);
retval.add(2+offset);
retval.add(1+offset);
retval.add(2+offset);
retval.add(3+offset);
  
  
  return retval;
}
public ArrayList<Vector2f> getFaceText()
{
 ArrayList<Vector2f> retval = new ArrayList<Vector2f>();  
retval.add(new Vector2f(1,0f));
retval.add(new Vector2f(0.5f,0f));

retval.add(new Vector2f(1f,0.3f));
retval.add(new Vector2f(0.5f,0.3f));

return retval;
} 
    
    public Face getFace(int index) {
    
       //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    int i = index;
    
    Face retval = new Face();
       if(UP)
    {
    retval.vertex.addAll(GetPositiveYVertex(position));
    retval.index.addAll(GetFaceIndex(i));
    retval.textures.addAll(getFaceText());
    i = index+ retval.vertex.size();
    }
    if(DOWN)
    {
    retval.vertex.addAll(GetNegativeYVertex(position));
    retval.index.addAll(GetFaceIndex(i));
    retval.textures.addAll(getFaceText());
    i =index+ retval.vertex.size();
    }
    if(FRONT)
    {
    retval.vertex.addAll(GetPositiveZVertex(position));
    retval.index.addAll(GetFaceIndex(i));
    retval.textures.addAll(getFaceText());
    i =index+ retval.vertex.size();
    }
    if(BACK)
    {
    retval.vertex.addAll(GetNegativeZVertex(position));
    retval.index.addAll(GetFaceIndex(i));
    retval.textures.addAll(getFaceText());
    i =index+ retval.vertex.size();
    }
    if(LEFT)
    {
    retval.vertex.addAll(GetPositiveXVertex(position));
    retval.index.addAll(GetFaceIndex(i));
    retval.textures.addAll(getFaceText());
    i =index + retval.vertex.size();
    }
    
    if(RIGHT)
    {
    retval.vertex.addAll(GetNegativeXVertex(position));
    retval.index.addAll(GetFaceIndex(i));
    retval.textures.addAll(getFaceText());
    i =index + retval.vertex.size();
    }
    return retval;
    }
    public void setMesh()
    {
       f = this.getFace(0);
       mesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(f.getVertex()));
       mesh.setBuffer(VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(f.getIndex()));
   // retval.setBuffer(VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(f.getNormals()));
       mesh.setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(f.getText()));
       mesh.setMode(Mesh.Mode.Triangles);
       mesh.updateBound();
     }
 public void setSize(Vector3f size)
 {
       sizeX = size.x;
       sizeY = size.y;
       sizeZ = size.z; 
 }
}
