/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import mygame.Terrain.Biomes.Tree;
import com.jme3.material.Material;
import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Box;
import com.jme3.system.JmeSystem;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;
import com.jme3.util.BufferUtils;
import com.jme3.util.TangentBinormalGenerator;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import javax.imageio.ImageIO;
import mygame.Terrain.BlockWorldSurface;
import mygame.Terrain.CallableSurface;
/**
 *
 * @author marko
 */

public class Player extends AbstractControl implements Callable<Boolean>  {
    private BlockWorldSurface  BWS = new BlockWorldSurface();
    private Node node = new Node();
    private ArrayList<Vector3f> positions = new ArrayList<Vector3f>();
    private NoiseTerrain geo = new NoiseTerrain(positions);
    private Geometry Geom;
    private Geometry TestGeom;
    private Material mat;
    private AssetManager assMag;
    private SimplexNoise Noise;
    private SimplexNoise NoiseBiome;
    private double step = 0.1;
    private int seed = 1665453136;
    private Vector3f[] Test;
    private ArrayList<Tree> trees = new  ArrayList<Tree>();
    double noise1(double nx, double ny) { return Noise.getNoise(nx,ny); }
    double noise2(double nx, double ny) { return NoiseBiome.getNoise(nx, ny); }
    private double e1,e2,e3,e4,e5,e6,e7,a,b,c;
    private double offsetX,offsetY;
    public Vector2f pos = new Vector2f(0,0);
    CallableSurface CallSurf = new CallableSurface(0,0,5000); 
    public void setParam(double... e)
    
    {
       // System.out.println(String.valueOf(e));
     //   for (int i=0 ;i<e.length; i++)
      //  {
    this.e1 = e[0];
    this.e3 = e[1];
    this.e3 = e[2];
    this.e4 = e[3];
    this.e5 = e[4];
    this.e6 = e[5];
     //System.out.println(String.valueOf(step));
    this.e7 = e[6];
    this.step = e[7];
    this.a = e[8];
    this.b = e[9];
    this.c = e[10];
    this.offsetX = e[11];
    this.offsetY = e[12];
      //  }
    
    
    }
    
    private double[][] writeTexture(int x,int y)
       {
           positions.clear();
           double xmin=0,xmax=0,ymin=0,ymax=0;
           
      double[][] result = new double[x][2*y];
      Test = new Vector3f[x*y];
      int index = 0;
      for(int i=0;i<x;i++){
        for(int j=0;j<y;j++){
      double nx = i*(0.03)-50,///(double)x), 
             ny = j*(0.03)-50;//(double)y),
             //nz = ((double)j)/step;//(double)y);
        double d = Math.sqrt(200);
       
         double e;
        // e=(1+noise1(nx,ny));
         // System.out.print(String.valueOf(nx) +" "+String.valueOf(ny) );
         //  System.out.println(String.valueOf(e));
            
      e = e1 * noise1(  nx,   ny)
              + e2 * noise1( Math.pow(1.618, Math.PI) * nx, Math.pow(1.618, Math.PI)* ny) 
              + e3 * noise1( Math.pow(1.618, 4) * nx,  Math.pow(1.618, 4) * ny) 
              + e4 * noise1( 8 * nx,  8 * ny) 
              + e5 * noise1(16 * nx, 16 * ny) 
              + e6 * noise1(32 * nx, 32 * ny);
           //  e = Math.pow(e, e7);
      e = e + a - (b*Math.pow(d,c));
           // if (e>2)
          // System.out.println(String.valueOf(e));
          //  if (e<-2)
          // System.out.println(String.valueOf(e));
            
           // e=  (e+2.5) /5;
           // e=e+0.2;
           
           // if (e>1)
          // System.out.println(String.valueOf(e));
          //  if (e<0)
          // System.out.println(String.valueOf(e));
           // e = Math.pow(e, e7);
           /*
   The general one-line formula to linearly rescale 
   data values having observed min and max into a 
   new arbitrary range min' to max' is

  newvalue= (1-0)/(2--2)*(value-2)+1
  or
  newvalue= (max'-min')/(max-min)*(value-min)+min'
            */
           
            
//e = e + a - b*d^c
   // e /= (e1+e2+e3+e4+e5+e6);

   //e = (e + 0.00) * (1 - 0.48*Math.pow(d, 2.30));
  //  e =  Math.round(e * 12) / 12;//
   /*double m = (0.5f * noise2( 1 * nx,  1 * ny)
             + 1f * noise2( 2 * nx,  2 * ny)
             + 0.84f * noise2( 4 * nx,  4 * ny)
             + 0.54f * noise2( 8 * nx,  8 * ny)
             + 0.28f * noise2(16 * nx, 16 * ny)
             + 0.39f * noise2(32 * nx, 32 * ny));
   */
   
   if (nx > xmax)
       xmax=nx;
   if (ny > ymax)
       ymax=ny;
   if (xmin > nx)
       xmin=nx;
   if (ymin > ny)
       ymin=ny;          
   
  // ny=ny +2*((x-x/2)*step);
   //System.out.println(String.valueOf(2*((x-x/2)*step)));
  nx = (double)i /1000f-50f;
   ny = (double)j/1000f-50f;
  double m = e1 * noise1(  nx,   ny)
              + e2 * noise1( 2 * nx,  2 * ny) 
              + e3 * noise1( 4 * nx,  4 * ny) 
              + e4 * noise1( 8 * nx,  8 * ny) 
              + e5 * noise1(16 * nx, 16 * ny) 
              + e6 * noise1(32 * nx, 32 * ny);
  m = m + a - b*Math.pow(d,c);   
  //e = e + a - b*Math.pow(d,c);
  // m = Math.pow(m, 1.4);
   // m /= (0.32+0.44+0.49+0.51+0.52+0.25);
             Test[index] = new Vector3f((float)i/0xa,(float)e,(float)j/0xa);
            index++;
        result[i][j]=e;
        positions.add(new Vector3f((float)i,(float)e,(float)j));
         result[i][j+x]=m;
        }
       
       }
       System.out.println(String.valueOf(positions.size()));
      System.out.println(String.valueOf(xmin)+" "+String.valueOf(xmax));
      System.out.println(String.valueOf(ymin)+" "+String.valueOf(ymax));
      return result;
    }
    Player(){}
    Player(AssetManager Ass){
    Noise = new SimplexNoise(1000,24.251,seed);
    NoiseBiome =  new SimplexNoise(1000,24.251,seed);
    this.assMag = Ass;
    Box box = new Box(3f,1f,2f);
    //e7 = 1.1;
    
    

     
    Image loader;
    loader = new AWTLoader().load(this.greyWriteImage(this.writeTexture(1,1)), false);
    
    Texture2D tex = new Texture2D(loader);
   // savePng(loader);
       
    mat = new Material(assMag,
          "Common/MatDefs/Misc/Unshaded.j3md");
    mat.setTexture("ColorMap", tex);
    Geom = new Geometry("Box",box);
    Mesh m;
    geo.Regenerate(positions);
  //  m.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(geo.getVertex()));
  //  m.setBuffer(Type.Index, 3, BufferUtils.createIntBuffer(geo.getIndex()));
  //  m.setBuffer(Type.Normal, 3, BufferUtils.createFloatBuffer(geo.getNormals()));
  //  m.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(geo.getText()));
    
    m = geo.getMesh();
    m.setMode(Mesh.Mode.Points);
    m.updateBound();
    TestGeom = new Geometry("Box",m);
//mat.setColor("Color",ColorRGBA.Blue);
    Geom.setMaterial(mat);
    TestGeom.setMaterial(mat);
    node.attachChild(Geom);
    node.attachChild(TestGeom);
    TestGeom.move(new Vector3f(10f,0.f,0.f));
    //Geometry Geom2 = Geom.clone();
   // Geom2.move(new Vector3f(10f,0.0f,10));
    //node.attachChild(Geom2);
    }
    
public void Color()
    {
    //mat.setColor("Color", ColorRGBA.randomColor());
    //this.seed++;
       // this.Noise.seed = (int)seed;
       // this.NoiseBiome = new OpenSimplexNoise(seed+1);
    Image loader;
    loader = new AWTLoader().load(this.greyWriteImage(this.writeTexture(100,100)), false);
    Texture2D tex = new Texture2D(loader);
   // tex.setWrap(Texture.WrapMode.EdgeClamp);
    
      //  assMag.loadTexture("Textures/grass.png"));
    BWS.generate(0,0);
    this.TestGeom.getMesh().clearBuffer(Type.Position);
    this.TestGeom.getMesh().clearBuffer(Type.Index);
    this.TestGeom.getMesh().clearBuffer(Type.Normal);
    this.TestGeom.getMesh().clearBuffer(Type.TexCoord);
    
    node.detachAllChildren();
    
    trees.clear();
    geo.Regenerate(positions);
    Mesh m = BWS.getPoints();
   // m.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(positions.toArray(new Vector3f[0])));
   // m.setBuffer(Type.Index, 3, BufferUtils.createIntBuffer(geo.getIndex()));
   // m.setBuffer(Type.Normal, 3, BufferUtils.createFloatBuffer(geo.getNormals()));
    //m.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(geo.getText()));
    
    
    m.setMode(Mesh.Mode.Points);
    m.updateBound();
    TestGeom.setMesh(m);
    
    
   // Material mat = this.TestGeom.getMaterial();
    //mat.
    this.Geom.getMaterial().setTexture("ColorMap", tex);
   Material mat_ = new Material(assMag,
          "Common/MatDefs/Misc/Unshaded.j3md");
    
   mat.setColor("Color", ColorRGBA.White);
    this.TestGeom.setMaterial(mat_);
   //this.TestGeom.getMaterial();
   node.attachChild(TestGeom);
   node.attachChild(Geom);

// m.updateBound();
   // TangentBinormalGenerator.generate(m);
   //trees  = new ArrayList<Tree>();
//   for(Vector3f e:positions)
//   {
//       if (trees.size() <5)
//       {
//   if(e.y > 0.6 && e.y < 0.8)
//   {
//     Tree  tree = new Tree().BuildTree(1f).BuildTree(assMag);
//       tree.Grow();
//       //tree.Grow();
//      // tree.Grow();
//       tree.CreateMesh();
//   trees.add(tree);
//   for (Geometry g : tree.body)
//   {
//   //g.setMaterial(mat);
//    
//        
//    g.move(e.add(new Vector3f(10f,0.f,0.f)));
//    
//    node.attachChild(g);
//   }
//   }
//   }
//   }
   
    
    }
    public void setRandom()
    {
   // seed = new Random().nextLong();
    }
    @Override
    public void setSpatial(Spatial spatial){
        Spatial oldSpatial = this.spatial;
        super.setSpatial(spatial);
        if(spatial instanceof Node){
            Node parentNode = (Node) spatial;
            parentNode.attachChild(node);
        }
        else if(oldSpatial instanceof Node){
            Node oldNode = (Node) oldSpatial;
            oldNode.detachChild(node);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Spatial getSpatial()
            {
                return this.node;
}
    
    @Override
    protected void controlRender(RenderManager renderManager, ViewPort viewPort){
        
    } 
   @Override
    protected void controlUpdate(float lastTimePerFrame){
       // updateSpatial();
       
    }
    private BufferedImage greyWriteImage(double[][] data){
        //this takes and array of doubles between 0 and 1 and generates a grey scale image from them

        BufferedImage image = new BufferedImage(data.length,data[0].length, BufferedImage.TYPE_INT_RGB);

        for (int y = 0; y < data[0].length; y++)
        {
          for (int x = 0; x < data.length; x++)
          {
          //  if (data[x][y]>1){
          //      data[x][y]=1;
          //  }
            /*if (data[x][y]<0){
                data[x][y]= data[x][y];
            }
              */
             //Color col = new Color(100,100,100); //Color col=new Color((float)data[x][y],(float)data[x][y],(float)data[x][y]); 
          // data[x][y]= data[x][y] +1;
         //  data[x][y]= data[x][y] /2;
              int rgb = 0x010101 * (int)((data[x][y] + 1) * 127.5);
            image.setRGB(x, y, rgb);
          }
        }
    //  try
   // {
    //  ImageIO.write(image, "png", new File("noise.png"));
  //  }
   //   catch(IOException ex){
    //    System.out.println (ex.toString());
    //    System.out.println("Could not find file " );
   // }
      
      return image; 
    }
       
public void savePng( Image img )  {
/*
    try
    {
    File f;
        f= new File("skrinsot.png");
        OutputStream out = new FileOutputStream(f);
        try {            
            JmeSystem.writeImageFile(out, "png", img.getData(0), 9, 9);  
        } finally {
            out.close();
        
        }
        
    }        
catch(IOException ex){
        System.out.println (ex.toString());
        System.out.println("Could not find file " );
    }
     */
    
    }

    

    @Override
    public Boolean call() throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    return true;
    }

}


