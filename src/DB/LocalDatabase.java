/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import mygame.Basic.Vector3I;
import mygame.Terrain.BlockTerrainChunk;

/**
 *
 * @author marko
 */
public class LocalDatabase {
    String LocaldbName;
    String LocaldbURL;
    private Connection conn = null;
     
    private static LocalDatabase instance = null;
private LocalDatabase() 
{
LocaldbName = "./BasicCube.db";    
LocaldbURL = "jdbc:sqlite:" + LocaldbName;    
    
    
File f = new File(LocaldbName);
if(!f.exists() && !f.isDirectory() ) { 
    createNewDatabase();
}
try {
            // db parameters
            
            // create a connection to the database
            conn = DriverManager.getConnection(LocaldbURL);
            
            System.out.println("Connection to SQLite has been established.");
            createNewDatabase();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } 
            catch (SQLException ex) {
                System.out.println(ex.getMessage());
                }                       
     
            }
    
}


public final static LocalDatabase getInstance() {
    return (instance != null) ? instance : makeInstance();
}
private static synchronized LocalDatabase makeInstance() {
    return ( instance != null ) ? instance : ( instance = new LocalDatabase() );
    
}    
 
    
    
    
    private void createNewDatabase() {
 
        
 
        try (Connection conn_ = DriverManager.getConnection(LocaldbURL)) {
            if (conn_ != null) {
                DatabaseMetaData meta = conn_.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
                  conn = conn_;
                  CreateTable();
            }
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    private void CreateTable()
    {
        String sql = "CREATE TABLE IF NOT EXISTS chunk (\n"
                + "	id integer,\n"
                + "	X integer,\n"
                + "	Y integer,\n"
                + "	Z integer,\n"
                + "PRIMARY KEY (id,X, Y, Z)\n"
                + ");";
    String sql1 = "CREATE TABLE IF NOT EXISTS block (\n"
                + "	X integer,\n"
                + "	Y integer,\n"
                + "	Z integer,\n"
                + "	chunkid integer ,\n"
                + "	PRIMARY KEY (X,Y,Z),\n"
                + "     FOREIGN KEY (chunkid) REFERENCES chunk (id)\n"
                + ");";
    String sql2 = "CREATE TABLE IF NOT EXISTS terrainSet (\n"
                + "	id integer,\n"
                + "	e0 integer,\n"
                + "	e1 integer,\n"
                + "	e2 integer,\n"
                + "     e3 integer,\n"
                + "     e4 integer,\n"
                + "     e5 integer,\n"
                + "     e6 integer,\n"
                + "	PRIMARY KEY (id)\n"
                + ");";
    try (Statement stmt = conn.createStatement();
            ){
            // create a new table
            stmt.execute(sql);
            stmt.execute(sql1);
            
            System.out.println("db inserted");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    
    
    
    }
    public void addChunk(BlockTerrainChunk ch)
    {
        String sql = "INSERT INTO "
                + "chunks"
                + "(ChunkId,X,Y,Z,content) "
                + "VALUES(?,?)";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
          //  pstmt.setString(1, ch.);
          //  pstmt.setDouble(2, capacity);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    
    }
    
}
